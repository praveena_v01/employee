﻿/****** Object:  DatabaseRole [db_execute]    Script Date: 7/15/2019 10:32:24 AM ******/
CREATE ROLE [db_execute]
GO
ALTER ROLE [db_execute] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_execute] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_execute] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_execute] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]