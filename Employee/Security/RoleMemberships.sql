﻿ALTER ROLE [db_owner] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_datareader] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [SEARS2\delderk]
GO
ALTER ROLE [db_datareader] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [os_spt]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_web]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_soprodev]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_dbconnect]
GO
ALTER ROLE [db_datareader] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [nfdt_boss]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\njain3]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDTTM-local]
GO
ALTER ROLE [db_owner] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datareader] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [KMART\HomeServices-HSNFDADMLocal]