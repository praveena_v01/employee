﻿CREATE VIEW [dbo].[VIEW_Profit]
AS

SELECT 
	*
FROM 
	Employee.dbo.profit_v2 WITH (NOLOCK)
WHERE
	acctg_yr = 2018
	AND acctg_mth > 6