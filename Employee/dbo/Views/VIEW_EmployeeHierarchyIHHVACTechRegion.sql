﻿CREATE VIEW [dbo].[VIEW_EmployeeHierarchyIHHVACTechRegion]
AS

	SELECT
		EnterpriseID
		,NPSID
		,FullName
		,TMDTMEnterpriseID
		,TMDTMFullName
		,DSMRTMEnterpriseID
		,DSMRTMFullName
		,Unit
		,UnitName
		,TFDEnterpriseID
		,TFDFullName
		,Territory
		,TerritoryName
		,Region
		,RegionName
		,RVPDirEnterpriseID
		,RVPDirFullName
	FROM
		Employee.dbo.TBL_EmployeeHierarchyIHHVACTechRegion WITH (NOLOCK)