﻿CREATE VIEW [dbo].[VIEW_EmployeeHierarchyIHTechRegion]
AS

	SELECT
            EnterpriseID
            ,NPSID
            ,FullName
            ,TMEnterpriseID
            ,TMFullName
            ,DSMEnterpriseID
            ,DSMFullName
            ,Unit
            ,UnitName
            ,TFDEnterpriseID
            ,TFDFullName
            ,TerritoryName
            ,Region
            ,RegionName
            ,RVPEnterpriseID
            ,RVPFullName
      FROM
            Employee.dbo.TBL_EmployeeHierarchyIHTechRegion WITH (NOLOCK)