﻿CREATE VIEW [dbo].[VIEW_EmployeeHierarchyIHUnitRegion]
AS

SELECT 
	Unit
	,UnitName
	,TFDEnterpriseID
	,TFDFullName
	,TerritoryName
	,Region
	,RegionName
	,RVPEnterpriseID
	,RVPFullName
FROM
	Employee.dbo.TBL_EmployeeHierarchyIHUnitRegion WITH (NOLOCK)