﻿CREATE VIEW [dbo].[VIEW_EmployeeTechHubPunchSummary]
AS
SELECT
	s.ServiceUnit AS un_no
	,e.NPSID AS emp_id
	,s.TechnicianEnterpriseID AS emp_racf
	,s.RouteDate AS rte_dt
	,s.StartDay AS pi_tm
	,s.StartPay AS sp_tm
	,NULL AS br_tm -- s.StartTruck AS br_tm
	,s.FirstSOStart AS fso_tm
	,s.StartLunch AS ln_sta_tm
	,s.EndLunch AS ln_end_tm
	,s.LastSOStart AS lso_tm
	,s.EndRoute AS er_tm
	,s.EndPay AS ep_tm
	,s.EndDay AS po_tm
	,ISNULL(d.bk_min, 0) AS bk_min
	,ISNULL(d.mt_min, 0) AS mt_min
	,ISNULL(d.tr_min, 0) AS tr_min
	,ISNULL(d.mc_min, 0) AS mc_min
	,s.LunchCount AS num_ln
	,GETDATE() AS insert_dt_tm
FROM
	(	SELECT
			p.RouteDate
			,p.ServiceUnit
			,p.TechnicianEnterpriseID
			,MIN(CASE WHEN p.EntryType = 'Start Day' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartDay
			,MIN(CASE WHEN p.EntryType = 'Start Pay' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartPay
			,MIN(CASE WHEN p.EntryType = 'Start Truck' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartTruck
			,MIN(CASE WHEN p.EntryType = 'Start Order' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS FirstSOStart
			,MIN(CASE WHEN p.EntryType = 'Start Lunch' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartLunch
			,MIN(CASE WHEN p.EntryType = 'End Lunch' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndLunch
			,MAX(CASE WHEN p.EntryType = 'End Order' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS LastSOStart
			,MIN(CASE WHEN p.EntryType = 'End Route' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndRoute
			,MIN(CASE WHEN p.EntryType = 'End Pay' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndPay
			,MIN(CASE WHEN p.EntryType = 'End Day' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndDay
			,MAX(CASE WHEN p.EntryType = 'Start Lunch' THEN p.EntryNumber ELSE 0 END) AS LunchCount
		FROM 
			Employee.dbo.TBL_EmployeeTechHubTimepunches p WITH (NOLOCK)
		WHERE
			p.RouteDate > '2018-12-29'
		GROUP BY
			p.RouteDate
			,p.ServiceUnit
			,p.TechnicianEnterpriseID	
	) s
LEFT JOIN
	Employee.dbo.TBL_Employee_New e WITH (NOLOCK)
	ON s.TechnicianEnterpriseID = e.EnterpriseID
LEFT JOIN
	(	SELECT
			d.ServiceUnit
			,d.TechnicianEnterpriseID
			,d.RouteDate
			,SUM(CASE WHEN d.EventCategory = 'Break' THEN d.Duration ELSE 0 END) AS bk_min
			,SUM(CASE WHEN d.EventCategory = 'Meeting' THEN d.Duration ELSE 0 END) AS mt_min
			,SUM(CASE WHEN d.EventCategory = 'Training' THEN d.Duration ELSE 0 END) AS tr_min
			,SUM(CASE WHEN d.EventCategory = 'Misc' THEN d.Duration ELSE 0 END) AS mc_min

			,SUM(CASE WHEN d.EventCategory = 'Lunch' THEN d.Duration ELSE 0 END) AS LunchTime
			,SUM(CASE WHEN d.EventCategory = 'Lunch' THEN 1 ELSE 0 END) AS LunchCount
			,SUM(CASE WHEN d.EventCategory = 'Meeting' THEN 1 ELSE 0 END) AS MeetingCount
			,SUM(CASE WHEN d.EventCategory = 'Training' THEN 1 ELSE 0 END) AS TrainingCount
			,SUM(CASE WHEN d.EventCategory = 'Order' THEN d.Duration ELSE 0 END) AS OrderTime
			,SUM(CASE WHEN d.EventCategory = 'Order' THEN 1 ELSE 0 END) AS OrderCount
			,SUM(CASE WHEN d.EventCategory = 'Misc' THEN 1 ELSE 0 END) AS MiscCount
			,SUM(CASE WHEN d.EventCategory = 'Break' THEN 1 ELSE 0 END) AS BreakCount
			,COUNT(*) AS Events
			,SUM(d.Duration) AS Duration
		FROM
			(	SELECT
					d.*
					,ISNULL(DATEDIFF(MINUTE, d.StartTime, d.EndTime), 0) AS Duration
				FROM
					(	SELECT
							p.RouteDate
							,p.ServiceUnit
							,p.TechnicianEnterpriseID
							,r.GroupType
							,r.EventCategory
							,p.EntryNumber
							,MIN(CASE WHEN LEFT(p.EntryType, 3) = 'Sta' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS StartTime
							,MAX(CASE WHEN LEFT(p.EntryType, 3) = 'End' THEN CAST(p.RouteDateTime AS TIME(0)) END) AS EndTime
						FROM 
							Employee.dbo.TBL_EmployeeTechHubTimepunches p WITH (NOLOCK)
						JOIN
							(	SELECT 
									*
								FROM
									Employee.dbo.TBL_EmployeeTechHubTimepunchReasons WITH (NOLOCK)
								WHERE
									IncludeEntry = 2
							) r
							ON p.EntryType = r.EntryType
						WHERE
							p.RouteDate > '2018-12-29'
						GROUP BY
							p.RouteDate
							,p.ServiceUnit
							,p.TechnicianEnterpriseID
							,r.GroupType
							,r.EventCategory
							,p.EntryNumber
					) d
			) d
		GROUP BY
			d.RouteDate
			,d.ServiceUnit
			,d.TechnicianEnterpriseID
	) d
	ON d.TechnicianEnterpriseID = s.TechnicianEnterpriseID
	AND d.RouteDate = s.RouteDate
	AND d.ServiceUnit = s.ServiceUnit