﻿CREATE VIEW [dbo].[VIEW_EmployeeHierarchyIHHVACUnitRegion]
AS

SELECT 
	Unit
	,UnitName
	,TFDEnterpriseID
	,TFDFullName
	,Territory
	,TerritoryName
	,Region
	,RegionName
	,RVPDirEnterpriseID
	,RVPDirFullName
FROM
	Employee.dbo.TBL_EmployeeHierarchyIHHVACUnitRegion WITH (NOLOCK)