﻿CREATE VIEW [dbo].[VIEW_CellControlVehicleLocationLatest]
AS
	SELECT 
		e.Unit
		,d.TechEnterpriseID
		,vl.VehicleDongleMacAddress
		,vl.PhoneNumber
		,vl.LocationTime 
		--,DATEADD(hour, DATEDIFF (hour, GETUTCDATE(), GETDATE()), LocationTime) AS LocationTime
		,vl.ManagerEnterpriseIDKey
		,vl.ManagerEnterpriseID
		,vl.ActiveTrip
		,vl.Longitude
		,vl.Latitude
		,vl.Heading
		,vl.Speed
		,vl.InsertDateTime
		,vl.ModifiedDateTime
	FROM 
		Employee.dbo.TBL_CellControlVehicleLocation vl with (nolock) 
	INNER JOIN
		(SELECT
			PhoneNumber
			,MAX(LocationTime) AS MaxLocationTime
		FROM 
			Employee.dbo.TBL_CellControlVehicleLocation with (nolock) 
		GROUP BY
			PhoneNumber) vlmax
	ON
		vl.PhoneNumber = vlmax.PhoneNumber
		AND	vl.LocationTime = vlmax.MaxLocationTime
JOIN 
	(	SELECT
			d.*
		FROM
			Employee.dbo.TBL_CellControlDeviceInformation as d with (nolock) 
		JOIN
			(	SELECT
					UserName
					,MAX(LastSeenDateTime) AS LST
				FROM 
					Employee.dbo.TBL_CellControlDeviceInformation
				GROUP BY
					UserName
			) l
			ON d.UserName = l.UserName
			AND d.LastSeenDateTime = l.LST
	) d
	on d.PhoneNumber = vl.PhoneNumber
JOIN 
	Employee.dbo.VIEW_Employee as e with (nolock) 
	ON e.EnterpriseID = d.TechEnterpriseID	
	WHERE 
		d.TechEnterpriseID <> ''
		AND vl.LocationTime >= CAST(getdate() as date)