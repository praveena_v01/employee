﻿CREATE TABLE [dbo].[TBL_PeoplesoftEmployee](
	[BusinessUnit] [varchar](3) NULL,
	[FinancialRegion] [varchar](5) NULL,
	[FinancialUnit] [varchar](5) NULL,
	[PayGroup] [varchar](5) NULL,
	[NID] [varchar](9) NULL,
	[EmployeeID] [varchar](11) NOT NULL,
	[FullName] [varchar](100) NULL,
	[EmployeeStatus] [varchar](1) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[StartDate] [date] NULL,
	[JobCode] [varchar](6) NULL,
	[JobTitle] [varchar](50) NULL,
	[JobStartDate] [date] NULL,
	[Address] [varchar](255) NULL,
	[Apartment] [varchar](60) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](2) NULL,
	[Zip] [varchar](50) NULL,
	[Gender] [varchar](1) NULL,
	[Department] [varchar](10) NULL,
	[EmployeeType] [varchar](1) NULL,
	[FullPartTime] [varchar](1) NULL,
	[SalaryPlan] [varchar](7) NULL,
	[JobGrade] [varchar](7) NULL,
	[PayType] [varchar](50) NULL,
	[GLDepartment] [varchar](50) NULL,
	[LastUpdate] [datetime] NULL,
 CONSTRAINT [PK_TBL_PeoplesoftEmployee] PRIMARY KEY NONCLUSTERED 
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]