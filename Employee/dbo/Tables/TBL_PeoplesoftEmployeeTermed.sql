﻿CREATE TABLE [dbo].[TBL_PeoplesoftEmployeeTermed](
	[BusinessUnit] [varchar](3) NULL,
	[FinancialRegion] [varchar](5) NULL,
	[FinancialUnit] [varchar](5) NULL,
	[PayGroup] [varchar](5) NULL,
	[EmployeeID] [varchar](11) NOT NULL,
	[FullName] [varchar](100) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[JobCode] [varchar](6) NULL,
	[JobTitle] [varchar](50) NULL,
	[StartDate] [date] NULL,
	[EmployeeStatus] [varchar](1) NULL,
	[TermedEffectiveDate] [date] NOT NULL,
	[ActionDate] [date] NULL,
	[ActionType] [varchar](3) NULL,
	[ActionReason] [varchar](3) NULL,
	[ActionDescription] [varchar](50) NULL,
	[LastUpdate] [datetime] NOT NULL,
 CONSTRAINT [PK_TBL_PeoplesoftEmployeeTermed] PRIMARY KEY CLUSTERED 
(
	[EmployeeID] ASC,
	[TermedEffectiveDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]