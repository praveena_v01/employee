﻿CREATE TABLE [dbo].[a_HR_Data](
	[PDC] [char](5) NULL,
	[Pay_group] [char](5) NULL,
	[empl_ID] [char](11) NULL,
	[LDAP_ID] [char](8) NULL,
	[Emp_Full_nm] [char](100) NULL,
	[status] [char](1) NULL,
	[job_code] [char](6) NULL,
	[job_title] [char](50) NULL,
	[assigment_date] [char](10) NULL,
	[dept_id] [char](10) NULL,
	[empl_type] [char](1) NULL,
	[full_part_time] [char](1) NULL,
	[salary_plan] [char](7) NULL,
	[grade] [char](7) NULL,
	[pay_rate] [numeric](11, 2) NULL,
	[pay_rate_save] [numeric](11, 2) NULL,
	[last_update] [datetime] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_HR_Data] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_HR_Data] ADD  CONSTRAINT [DF__a_HR_Data__LastU__640DD89F]  DEFAULT (getdate()) FOR [LastUpdated]