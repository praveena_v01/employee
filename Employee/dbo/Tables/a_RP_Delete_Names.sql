﻿CREATE TABLE [dbo].[a_RP_Delete_Names](
	[FullName] [char](75) NULL,
	[PDC] [char](4) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Delete_Names] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_RP_Delete_Names] ADD  CONSTRAINT [DF__a_RP_Dele__LastU__6F7F8B4B]  DEFAULT (getdate()) FOR [LastUpdated]