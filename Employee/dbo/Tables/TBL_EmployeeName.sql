﻿CREATE TABLE [dbo].[TBL_EmployeeName](
	[UniqueID] [int] NOT NULL,
	[CompanyCode] [varchar](3) NULL,
	[FinancialRegion] [char](7) NULL,
	[FinancialUnitNumber] [char](7) NULL,
	[BusinessUnitNumber] [char](7) NULL,
	[PayGroup] [varchar](5) NULL,
	[NID] [varchar](9) NULL,
	[StatusCode] [char](1) NULL,
	[EnterpriseID] [varchar](10) NULL,
	[JobCode] [varchar](6) NULL,
	[JobTitle] [varchar](70) NULL,
	[AssignmentDate] [date] NULL,
	[HireDate] [date] NULL,
	[TermDate] [date] NULL,
	[EmployeeFullName] [varchar](250) NULL,
	[EmployeeFirstName] [varchar](100) NULL,
	[EmployeeLastName] [varchar](100) NULL,
	[EmployeeDisplayName] [varchar](250) NULL,
	[ManagerEnterpriseID] [varchar](10) NULL,
	[AddressLine1] [varchar](250) NULL,
	[AddressLine2] [varchar](250) NULL,
	[City] [varchar](100) NULL,
	[State] [varchar](2) NULL,
	[ZipCode] [varchar](9) NULL,
	[Gender] [char](1) NULL,
	[DepartmentID] [varchar](10) NULL,
	[EmployeeType] [char](1) NULL,
	[FullTime] [int] NULL,
	[SalaryPlan] [varchar](7) NULL,
	[JobGrade] [varchar](7) NULL,
	[PayType] [varchar](3) NULL,
	[DepartmentCode] [varchar](10) NULL,
	[WorkPhone] [char](10) NULL,
	[HomePhone] [char](10) NULL,
	[MobilePhone] [char](10) NULL,
	[EmailAddress] [varchar](254) NULL,
	[LastUpdate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeName] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_EmployeeName_EnterpriseID]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_EmployeeName_EnterpriseID] ON [dbo].[TBL_EmployeeName]
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]