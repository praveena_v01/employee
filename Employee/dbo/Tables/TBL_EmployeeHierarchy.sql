﻿CREATE TABLE [dbo].[TBL_EmployeeHierarchy](
	[EnterpriseID] [varchar](28) NOT NULL,
	[FullName] [varchar](250) NULL,
	[ManagerEnterpriseID] [varchar](28) NULL,
	[ManagerFullName] [varchar](250) NULL,
	[ManagerJobCode] [varchar](10) NULL,
	[ManagerJobTitle] [varchar](70) NULL,
	[ManagerTwoEnterpriseID] [varchar](28) NULL,
	[ManagerTwoFullName] [varchar](250) NULL,
	[ManagerTwoJobCode] [varchar](10) NULL,
	[ManagerTwoJobTitle] [varchar](70) NULL,
	[ManagerThreeEnterpriseID] [varchar](28) NULL,
	[ManagerThreeFullName] [varchar](250) NULL,
	[ManagerThreeJobCode] [varchar](10) NULL,
	[ManagerThreeJobTitle] [varchar](70) NULL,
	[ManagerFourEnterpriseID] [varchar](28) NULL,
	[ManagerFourFullName] [varchar](250) NULL,
	[ManagerFourJobCode] [varchar](10) NULL,
	[ManagerFourJobTitle] [varchar](70) NULL,
	[ManagerFiveEnterpriseID] [varchar](28) NULL,
	[ManagerFiveFullName] [varchar](250) NULL,
	[ManagerFiveJobCode] [varchar](10) NULL,
	[ManagerFiveJobTitle] [varchar](70) NULL,
 CONSTRAINT [PK_TBL_EmployeeHierarchy] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]