﻿CREATE TABLE [dbo].[a_ForeCast_Actual_Factors](
	[PK] [int] IDENTITY(1,1) NOT NULL,
	[PDC] [char](5) NULL,
	[Mth] [char](3) NULL,
	[month_nmbr] [int] NULL,
	[Year] [int] NULL,
	[Forecast_PR_Benefit_Cost] [numeric](18, 2) NULL,
	[Actual_PR_Benefit_Cost] [numeric](18, 2) NULL,
	[Forecast_Support_PR_Cost] [numeric](18, 2) NULL,
	[Actual_Support_PR_Cost] [numeric](18, 2) NULL,
	[ForeCast_Tot_PR_Cost] [numeric](18, 2) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
 CONSTRAINT [PK_a_ForeCast_Actual_Factors] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_ForeCast_Actual_Factors] ADD  CONSTRAINT [DF_a_ForeCast_Actual_Factors_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]