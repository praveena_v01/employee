﻿CREATE TABLE [dbo].[Employee_Info](
	[Company] [varchar](3) NULL,
	[Fin_Rgn_Un_No] [varchar](5) NULL,
	[Fin_Ctr_Svc_Un_No] [varchar](5) NULL,
	[Pay_Group] [varchar](5) NULL,
	[NID] [varchar](9) NULL,
	[Empl_ID] [varchar](11) NOT NULL,
	[Emp_Full_Nm] [varchar](100) NULL,
	[Status] [varchar](1) NULL,
	[LDAP_ID] [varchar](8) NULL,
	[Hire_Date] [date] NULL,
	[Job_Code] [varchar](6) NULL,
	[Job_Title] [varchar](50) NULL,
	[Assignment_Date] [date] NULL,
	[Address] [varchar](255) NULL,
	[Apt] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[St] [varchar](2) NULL,
	[Zip] [varchar](50) NULL,
	[Gender] [varchar](1) NULL,
	[Dept_ID] [varchar](10) NULL,
	[Empl_Type] [varchar](1) NULL,
	[Full_Part_Time] [varchar](1) NULL,
	[Salary_Plan] [varchar](7) NULL,
	[Grade] [varchar](7) NULL,
	[GL_Pay_Type] [varchar](50) NULL,
	[K_GL_Dept] [varchar](50) NULL,
	[Pay_Rate] [varbinary](256) NULL,
	[Last_Update] [datetime] NULL,
 CONSTRAINT [PK_Employee_Info] PRIMARY KEY NONCLUSTERED 
(
	[Empl_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]