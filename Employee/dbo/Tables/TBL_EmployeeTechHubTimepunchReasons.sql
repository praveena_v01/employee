﻿CREATE TABLE [dbo].[TBL_EmployeeTechHubTimepunchReasons](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EntryType] [varchar](50) NULL,
	[GroupType] [varchar](50) NULL,
	[IncludeEntry] [int] NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[EventCategory] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_EmployeeTechHubTimepunchReasons] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_EmployeeTechHubTimepunchReasons] ADD  CONSTRAINT [DF_TBL_EmployeeTechHubTimepunchReasons]  DEFAULT (getdate()) FOR [CreatedDateTime]