﻿CREATE TABLE [dbo].[TBL_CellControlVehicleInformation](
	[VehicleDongleMacAddress] [varchar](12) NOT NULL,
	[VIN] [varchar](20) NULL,
	[CustomID] [varchar](30) NULL,
	[ActivationID] [varchar](20) NULL,
	[Firmware] [varchar](20) NULL,
	[Voltage] [decimal](18, 4) NULL,
	[LastSeenDateTimeUTC] [datetime] NULL,
	[OrientationX] [decimal](18, 4) NULL,
	[OrientationY] [decimal](18, 4) NULL,
	[OrientationZ] [decimal](18, 4) NULL,
	[LastSeenDateTime] [datetime] NULL,
	[LastPhoneNumber] [varchar](20) NULL,
	[Company] [varchar](30) NULL,
	[EntityID] [int] NULL,
	[TechManagerGroupID] [int] NULL,
	[TechManagerEnterpriseID] [varchar](50) NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_CellControlVehicleInformation] PRIMARY KEY CLUSTERED 
(
	[VehicleDongleMacAddress] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_CellControlVehicleInformation] ADD  CONSTRAINT [DF_TBL_CellControlVehicleInformation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]