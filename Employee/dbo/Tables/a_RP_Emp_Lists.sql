﻿CREATE TABLE [dbo].[a_RP_Emp_Lists](
	[name] [char](75) NULL,
	[badge_number] [char](15) NULL,
	[Phone] [char](12) NULL,
	[Emp_Type] [char](15) NULL,
	[Hire_Date] [char](12) NULL,
	[Department] [char](35) NULL,
	[Primary_Job] [char](35) NULL,
	[shift_Strategy] [char](15) NULL,
	[PDC] [char](5) NULL,
	[date] [char](10) NULL,
	[LDAP_ID] [char](8) NULL,
	[fname] [char](15) NULL,
	[lname] [char](20) NULL,
	[head_Cnt_Code] [char](7) NULL,
	[type_emp] [char](20) NULL,
	[shift] [char](6) NULL,
	[Head_Count_Code] [char](4) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Emp_Lists] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_RP_Emp_Lists] ADD  CONSTRAINT [DF__a_RP_Emp___LastU__753864A1]  DEFAULT (getdate()) FOR [LastUpdated]