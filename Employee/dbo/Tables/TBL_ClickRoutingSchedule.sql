﻿CREATE TABLE [dbo].[TBL_ClickRoutingSchedule](
	[TaskKey] [bigint] NULL,
	[AssignmentEventKey] [int] NULL,
	[ExternalReferenceID] [varchar](64) NULL,
	[CallID] [varchar](64) NOT NULL,
	[CallNumber] [int] NOT NULL,
	[TaskStatusCode] [int] NULL,
	[TaskStatus] [varchar](50) NULL,
	[AttemptType] [varchar](50) NULL,
	[TaskEmployeeID] [varchar](20) NULL,
	[AssignEmployeeID] [varchar](20) NULL,
	[TaskType] [varchar](25) NULL,
	[ServiceUnit] [char](7) NULL,
	[ServiceOrder] [char](8) NULL,
	[VisitNumber] [int] NULL,
	[ScheduleDate] [date] NULL,
	[CustomerRequestedStartDate] [datetimeoffset](0) NULL,
	[CustomerRequestedEndDate] [datetimeoffset](0) NULL,
	[ScheduledStartDate] [datetimeoffset](0) NULL,
	[ScheduledEndDate] [datetimeoffset](0) NULL,
	[ActualStartDate] [datetimeoffset](0) NULL,
	[ActualEndDate] [datetimeoffset](0) NULL,
	[CustomerName] [varchar](64) NULL,
	[CustomerEmail] [varchar](254) NULL,
	[CustomerPhone] [char](10) NULL,
	[EventAddress] [varchar](100) NULL,
	[EventCity] [varchar](30) NULL,
	[EventState] [char](2) NULL,
	[EventZipode] [varchar](9) NULL,
	[EventLocation] [geography] NULL,
	[EventLatitude]  AS ([EventLocation].[Lat]),
	[EventLongitude]  AS ([EventLocation].[Long]),
	[RestrictedLocation] [int] NULL,
	[Market] [int] NULL,
	[SubMarket] [int] NULL,
	[AssignmentTypeCode] [int] NOT NULL,
	[EventDescription] [varchar](max) NULL,
	[PureDuration] [int] NULL,
	[AdditionalTravelDuration] [int] NULL,
	[AdditionalUpsellDuration] [int] NULL,
	[PlannedDuration] [int] NULL,
	[ActualDuration] [int] NULL,
	[PlannedTravelDuration] [int] NULL,
	[ActualTravelDuration] [int] NULL,
	[Calendar] [int] NULL,
	[UnionCode] [int] NULL,
	[ApprovedByEmployeeID] [varchar](20) NULL,
	[ApprovedDate] [datetimeoffset](0) NULL,
	[TaskCreateDate] [datetimeoffset](0) NULL,
	[TaskExternalLastModifiedDate] [datetimeoffset](0) NULL,
	[TaskInternalLastModifiedDate] [datetimeoffset](0) NULL,
	[AssignmentCreateDate] [datetimeoffset](0) NULL,
	[AssignmentExternalLastModifiedDate] [datetimeoffset](0) NULL,
	[AssignmentInternalLastModifiedDate] [datetimeoffset](0) NULL,
	[AssignmentStatus] [int] NULL,
 CONSTRAINT [PK__TBL_ClickRoutingSchedule] PRIMARY KEY CLUSTERED 
(
	[CallID] ASC,
	[CallNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_ClickRoutingSchedule] ADD  DEFAULT ((-1)) FOR [CallNumber]
GO
ALTER TABLE [dbo].[TBL_ClickRoutingSchedule] ADD  DEFAULT ((-1)) FOR [AssignmentTypeCode]
GO
/****** Object:  Index [IDX_TBL_ClickRoutingSchedule_UnitServiceOrder]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IDX_TBL_ClickRoutingSchedule_UnitServiceOrder] ON [dbo].[TBL_ClickRoutingSchedule]
(
	[ServiceUnit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]