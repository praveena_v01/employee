﻿CREATE TABLE [dbo].[TBL_PaySummaryEndOfWeek](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [char](11) NOT NULL,
	[PayGroup] [char](5) NOT NULL,
	[WeekEndDate] [date] NULL,
	[Department] [char](10) NULL,
	[JobCode] [char](6) NULL,
	[JobTitle] [varchar](50) NULL,
	[Description] [varchar](50) NULL,
	[PayrollHours] [decimal](9, 4) NULL,
	[EarnDateTime] [datetime2](7) NULL,
	[EarnDate] [date] NULL,
	[EarnCode] [char](3) NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
	[LastModifiedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_PaySummaryEndOfWeek] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_PaySummaryEndOfWeek] ADD  CONSTRAINT [DF_TBL_PaySummaryEndOfWeek_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
ALTER TABLE [dbo].[TBL_PaySummaryEndOfWeek] ADD  CONSTRAINT [DF_TBL_PaySummaryEndOfWeek_LastModifiedDateTime]  DEFAULT (getdate()) FOR [LastModifiedDateTime]