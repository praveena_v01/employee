﻿CREATE TABLE [dbo].[TBL_EmployeeTechHubTimepunches](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[RouteDate] [date] NULL,
	[RouteDateTime] [datetime2](7) NULL,
	[TimeZone] [varchar](5) NULL,
	[UserEnterpriseID] [varchar](28) NULL,
	[TechnicianEnterpriseID] [varchar](28) NULL,
	[ServiceUnit] [varchar](7) NULL,
	[EntryType] [varchar](50) NULL,
	[EntryNumber] [varchar](50) NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_TBL_EmployeeTechHubTimepunches] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_EmployeeTechHubTimepunches] ADD  CONSTRAINT [DF_TBL_EmployeeTechHubTimepunches_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]
GO
/****** Object:  Index [IX_TBL_EmployeeTechHubTimepunches_TechnicianEnterpriseID_RouteDateTime_EntryType]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [IX_TBL_EmployeeTechHubTimepunches_TechnicianEnterpriseID_RouteDateTime_EntryType] ON [dbo].[TBL_EmployeeTechHubTimepunches]
(
	[TechnicianEnterpriseID] ASC,
	[RouteDateTime] ASC,
	[EntryType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]