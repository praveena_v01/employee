﻿CREATE TABLE [dbo].[STG_EmployeeTechHubTimepunches](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[RouteDate] [date] NULL,
	[RouteDateTime] [datetime2](7) NULL,
	[TimeZone] [varchar](5) NULL,
	[UserEnterpriseID] [varchar](28) NULL,
	[TechnicianEnterpriseID] [varchar](28) NULL,
	[ServiceUnit] [varchar](7) NULL,
	[EntryType] [varchar](50) NULL,
	[EntryNumber] [varchar](50) NULL,
	[CreatedDateTime] [datetime2](7) NOT NULL,
 CONSTRAINT [PK_STG_EmployeeTechHubTimepunches] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[STG_EmployeeTechHubTimepunches] ADD  CONSTRAINT [DF_STG_EmployeeTechHubTimepunches_CreatedDateTime]  DEFAULT (getdate()) FOR [CreatedDateTime]