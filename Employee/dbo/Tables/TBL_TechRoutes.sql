﻿CREATE TABLE [dbo].[TBL_TechRoutes](
	[Unit] [char](7) NOT NULL,
	[TechID] [char](7) NOT NULL,
	[EnterpriseID] [varchar](10) NOT NULL,
	[RouteDate] [date] NOT NULL,
	[ServiceOrder] [char](8) NOT NULL,
	[StatusCode] [varchar](15) NULL,
	[ScheduleFromTime] [datetime] NULL,
	[ScheduleToTime] [datetime] NULL,
	[EstimatedStart] [datetime] NULL,
	[LastUpdate] [datetime] NULL
) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_TechRoutes_EnterpriseIDRouteDate]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_TechRoutes_EnterpriseIDRouteDate] ON [dbo].[TBL_TechRoutes]
(
	[EnterpriseID] ASC,
	[RouteDate] ASC
)
INCLUDE ( 	[Unit],
	[TechID],
	[ServiceOrder],
	[StatusCode],
	[ScheduleFromTime],
	[ScheduleToTime],
	[EstimatedStart],
	[LastUpdate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_TechRoutes_UnitServiceOrder]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_TechRoutes_UnitServiceOrder] ON [dbo].[TBL_TechRoutes]
(
	[Unit] ASC,
	[ServiceOrder] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]