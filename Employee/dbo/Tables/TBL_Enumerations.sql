﻿CREATE TABLE [dbo].[TBL_Enumerations](
	[LineID] [int] IDENTITY(1,1) NOT NULL,
	[ParentLineID] [int] NULL,
	[Business] [varchar](20) NOT NULL,
	[OwnerType] [varchar](20) NOT NULL,
	[OwnerID] [int] NULL,
	[ShortDescription] [varchar](50) NULL,
	[LongDescription] [varchar](500) NULL,
	[NumericValue] [int] NULL,
	[SortOrder] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IsUserAllowed] [bit] NOT NULL,
	[AncillaryShort] [varchar](50) NULL,
	[AncillaryNumeric] [int] NULL,
	[AncillaryLong] [varchar](max) NULL,
	[BusinessNumericValue] [int] NULL,
	[LastModified] [datetime2](0) NOT NULL,
	[LastModifiedBy] [varchar](10) NULL,
 CONSTRAINT [PK_TBL_Enumerations] PRIMARY KEY NONCLUSTERED 
(
	[LineID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_Enumerations] ADD  DEFAULT ((0)) FOR [SortOrder]
GO
ALTER TABLE [dbo].[TBL_Enumerations] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[TBL_Enumerations] ADD  DEFAULT ((1)) FOR [IsUserAllowed]
GO
ALTER TABLE [dbo].[TBL_Enumerations] ADD  DEFAULT (getdate()) FOR [LastModified]