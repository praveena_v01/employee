﻿CREATE TABLE [dbo].[TBL_CellControlVehicleLocation](
	[VehicleDongleMacAddress] [varchar](12) NOT NULL,
	[PhoneNumber] [varchar](15) NULL,
	[LocationTime] [datetime] NOT NULL,
	[ManagerEnterpriseIDKey] [int] NULL,
	[ManagerEnterpriseID] [varchar](11) NULL,
	[ActiveTrip] [bit] NULL,
	[Longitude] [decimal](10, 5) NULL,
	[Latitude] [decimal](10, 5) NULL,
	[Heading] [decimal](10, 5) NULL,
	[Speed] [decimal](10, 5) NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_CellControlVehicleLocation] PRIMARY KEY CLUSTERED 
(
	[VehicleDongleMacAddress] ASC,
	[LocationTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_CellControlVehicleLocation] ADD  CONSTRAINT [DF_TBL_CellControlVehicleLocation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]