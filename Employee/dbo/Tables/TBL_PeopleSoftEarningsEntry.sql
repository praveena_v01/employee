﻿CREATE TABLE [dbo].[TBL_PeopleSoftEarningsEntry](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[Region] [char](5) NULL,
	[Unit] [char](5) NULL,
	[Location] [char](5) NULL,
	[ManagerName] [varchar](30) NULL,
	[EmployeeID] [char](11) NOT NULL,
	[EmployeeName] [varchar](30) NULL,
	[EnterpriseID] [varchar](8) NULL,
	[JobCode] [char](6) NULL,
	[JobTitle] [varchar](30) NULL,
	[DepartmentID] [char](10) NULL,
	[Division] [char](4) NULL,
	[Endweekdate] [date] NOT NULL,
	[EarnCode] [char](3) NOT NULL,
	[EarnDescription] [varchar](30) NULL,
	[GLExpense] [varchar](11) NULL,
	[MultFactor] [decimal](8, 5) NULL,
	[CompRate] [decimal](16, 4) NULL,
	[Hours] [decimal](16, 4) NULL,
	[OthPy] [decimal](16, 4) NULL,
	[Dollars] [decimal](16, 4) NULL,
 CONSTRAINT [PK_TBL_PeopleSoftEarningsEntry] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]