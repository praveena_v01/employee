﻿CREATE TABLE [dbo].[TBL_EmployeeHierarchyIHHVACTechRegion](
	[EnterpriseID] [varchar](10) NOT NULL,
	[NPSID] [varchar](7) NULL,
	[FullName] [varchar](250) NULL,
	[TMDTMEnterpriseID] [varchar](10) NULL,
	[TMDTMFullName] [varchar](250) NULL,
	[DSMRTMEnterpriseID] [varchar](10) NULL,
	[DSMRTMFullName] [varchar](250) NULL,
	[Unit] [char](7) NULL,
	[UnitName] [varchar](250) NULL,
	[TFDEnterpriseID] [varchar](10) NULL,
	[TFDFullName] [varchar](250) NULL,
	[Territory] [char](7) NULL,
	[TerritoryName] [varchar](250) NULL,
	[Region] [char](7) NULL,
	[RegionName] [varchar](250) NULL,
	[RVPDirEnterpriseID] [varchar](10) NULL,
	[RVPDirFullName] [varchar](250) NULL,
 CONSTRAINT [PK_TBL_EmployeeHierarchyIHHVACTechRegion] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]