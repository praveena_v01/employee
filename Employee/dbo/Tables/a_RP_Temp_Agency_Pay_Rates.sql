﻿CREATE TABLE [dbo].[a_RP_Temp_Agency_Pay_Rates](
	[PAY_RATE] [numeric](10, 2) NULL,
	[Effective_Date] [datetime] NULL,
	[TYPE_EMP] [char](20) NULL,
	[PDC] [char](4) NULL,
	[Previous_Rate] [numeric](10, 2) NULL,
	[Previous_Eff_Date] [datetime] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Temp_Agency_Pay_Rates] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_RP_Temp_Agency_Pay_Rates] ADD  CONSTRAINT [DF__a_RP_Temp__LastU__0C1BC9F9]  DEFAULT (getdate()) FOR [LastUpdated]