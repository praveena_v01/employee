﻿CREATE TABLE [dbo].[TBL_EmployeeTechHub](
	[EmployeeID] [varchar](14) NOT NULL,
	[EnterpriseID] [varchar](28) NULL,
	[Region] [varchar](7) NOT NULL,
	[FinancialUnit] [varchar](5) NOT NULL,
	[Unit] [varchar](30) NOT NULL,
	[NPSID] [varchar](7) NOT NULL,
	[FullName] [varchar](250) NOT NULL,
	[JobCode] [varchar](10) NOT NULL,
	[TechHubStartDate] [date] NULL
) ON [PRIMARY]