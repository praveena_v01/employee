﻿CREATE TABLE [dbo].[a_Badge_ID_LDAP_ID_Pay_Rates](
	[PDC] [char](5) NULL,
	[Empl_ID] [char](15) NULL,
	[LDAP_ID] [char](8) NULL,
	[Pay_rate] [numeric](10, 2) NULL,
	[Pay_rate_save] [numeric](10, 2) NULL,
	[Last_update] [datetime] NULL,
	[last_pay_raise] [datetime] NULL,
	[Head_Cnt_Code] [char](6) NULL,
	[Type_Emp] [char](20) NULL,
	[fname] [varchar](50) NULL,
	[lname] [varchar](50) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Badge_ID_LDAP_ID_Pay_Rates] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Badge_ID_LDAP_ID_Pay_Rates] ADD  CONSTRAINT [DF__a_Badge_I__LastU__473C8FC7]  DEFAULT (getdate()) FOR [LastUpdated]