﻿CREATE TABLE [dbo].[TBL_SSTTechnician](
	[Unit] [varchar](7) NULL,
	[Location] [varchar](5) NULL,
	[EnterpriseID] [varchar](8) NOT NULL,
	[EmployeeID] [varchar](11) NULL,
	[NPSID] [varchar](7) NULL,
	[IPAddress] [varchar](20) NULL,
	[CurrentSoftwareVersion] [int] NULL,
	[PendingSoftwareVersion] [int] NULL,
	[SocialSecurityNumber] [varchar](9) NULL,
	[SupportDataDownloaded] [varchar](1) NULL,
	[TimeZoneFactor] [smallint] NULL,
	[SendAutoSyn] [varchar](1) NULL,
	[WLSSyn] [varchar](1) NULL,
	[HAL] [varchar](1) NULL,
	[ULOLogDate] [char](10) NULL,
	[CSAT] [varchar](1) NULL,
	[SYWR] [varchar](1) NULL,
	[LMC] [varchar](1) NULL,
	[EMROORD] [varchar](1) NULL,
	[AIM] [varchar](1) NULL,
	[PRLotto] [varchar](1) NULL,
	[LMCDMZ] [varchar](1) NULL,
	[LMCTCP] [varchar](1) NULL,
	[NewSST] [varchar](1) NULL,
	[TI] [varchar](1) NULL,
 CONSTRAINT [PK_TBL_SSTTechnician] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]