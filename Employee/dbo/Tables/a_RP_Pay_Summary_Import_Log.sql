﻿CREATE TABLE [dbo].[a_RP_Pay_Summary_Import_Log](
	[WeekEndingDate] [datetime] NULL,
	[_8842_RptRuntimeStamp] [datetime] NULL,
	[_8940_RptRuntimeStamp] [datetime] NULL,
	[_8961_RptRuntimeStamp] [datetime] NULL,
	[_8963_RptRuntimeStamp] [datetime] NULL,
	[_8976_RptRuntimeStamp] [datetime] NULL,
	[_8966_RptRuntimeStamp] [datetime] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Pay_Summary_Import_Log] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_RP_Pay_Summary_Import_Log] ADD  CONSTRAINT [DF__a_RP_Pay___LastU__00AA174D]  DEFAULT (getdate()) FOR [LastUpdated]