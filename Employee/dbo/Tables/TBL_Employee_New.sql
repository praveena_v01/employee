﻿CREATE TABLE [dbo].[TBL_Employee_New](
	[UniqueID] [uniqueidentifier] NOT NULL,
	[BusinessUnit] [varchar](4) NOT NULL,
	[FinancialRegion] [varchar](5) NOT NULL,
	[FinancialUnit] [varchar](5) NOT NULL,
	[Region] [varchar](7) NOT NULL,
	[Unit] [varchar](30) NOT NULL,
	[Department] [varchar](50) NULL,
	[SearsDepartment] [varchar](50) NULL,
	[SearsDepartmentName] [varchar](30) NOT NULL,
	[GLDepartment] [varchar](50) NOT NULL,
	[Location] [varchar](5) NOT NULL,
	[LocationDescription] [varchar](30) NOT NULL,
	[PayType] [varchar](3) NOT NULL,
	[PayGroup] [varchar](7) NULL,
	[JobGrade] [varchar](5) NOT NULL,
	[EnterpriseID] [varchar](28) NOT NULL,
	[EmployeeID] [varchar](14) NOT NULL,
	[NPSID] [varchar](7) NOT NULL,
	[SalesID] [varchar](7) NOT NULL,
	[EmployeeStatus] [varchar](1) NOT NULL,
	[EmployeeType] [varchar](1) NOT NULL,
	[LastName] [varchar](100) NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[MiddleName] [varchar](50) NOT NULL,
	[FullName] [varchar](250) NOT NULL,
	[DisplayName] [varchar](250) NOT NULL,
	[Email] [varchar](250) NOT NULL,
	[OfficePhone] [varchar](70) NOT NULL,
	[MobilePhone] [varchar](70) NOT NULL,
	[MobilePhoneProvider] [varchar](10) NOT NULL,
	[JobCode] [varchar](10) NOT NULL,
	[JobTitle] [varchar](71) NOT NULL,
	[StartDate] [varchar](10) NOT NULL,
	[JobStartDate] [varchar](10) NOT NULL,
	[TermDate] [varchar](10) NOT NULL,
	[ManagerEnterpriseID] [varchar](28) NOT NULL,
	[ManagerEmployeeID] [varchar](14) NOT NULL,
	[ManagerStartDate] [date] NOT NULL,
	[RowStartDate] [date] NOT NULL,
	[RowEndDate] [date] NULL,
 CONSTRAINT [PK__TBL_Empl__308F3E580D44F85C] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_Employee_New_EnterpriseID]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Employee_New_EnterpriseID] ON [dbo].[TBL_Employee_New]
(
	[EnterpriseID] ASC
)
INCLUDE ( 	[FullName],
	[Email],
	[OfficePhone],
	[MobilePhone]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_Employee_New_NPSID_Unit]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_Employee_New_NPSID_Unit] ON [dbo].[TBL_Employee_New]
(
	[NPSID] ASC,
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]