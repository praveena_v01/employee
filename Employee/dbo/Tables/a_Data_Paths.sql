﻿CREATE TABLE [dbo].[a_Data_Paths](
	[PDC] [char](4) NULL,
	[rpt_path_daily] [char](100) NULL,
	[rpt_path_arhive] [char](100) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Data_Paths] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Data_Paths] ADD  CONSTRAINT [DF__a_Data_Pa__LastU__4959E263]  DEFAULT (getdate()) FOR [LastUpdated]