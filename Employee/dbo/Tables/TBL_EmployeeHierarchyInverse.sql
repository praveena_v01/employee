﻿CREATE TABLE [dbo].[TBL_EmployeeHierarchyInverse](
	[ManagerEnterpriseID] [varchar](28) NOT NULL,
	[ManagerFullName] [varchar](250) NULL,
	[EnterpriseID] [varchar](28) NOT NULL,
	[FullName] [varchar](250) NULL,
	[LevelDown] [int] NULL
) ON [PRIMARY]