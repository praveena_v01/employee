﻿CREATE TABLE [dbo].[a_RP_Pay_Summary](
	[weekendingdate] [datetime] NULL,
	[rptruntimestamp] [datetime] NULL,
	[date] [datetime] NULL,
	[pdc] [char](5) NULL,
	[pay_id] [char](15) NULL,
	[ldap_id] [char](8) NULL,
	[fname] [char](20) NULL,
	[lname] [char](20) NULL,
	[head_cnt_code] [char](6) NULL,
	[type_emp] [char](20) NULL,
	[tot_hrs] [numeric](10, 2) NULL,
	[reg_hrs] [numeric](10, 2) NULL,
	[tmp_hrs] [numeric](10, 2) NULL,
	[ovt_hrs] [numeric](10, 2) NULL,
	[dbl_OT_hrs] [numeric](10, 2) NULL,
	[ben_hrs] [numeric](10, 2) NULL,
	[tot_pay] [numeric](10, 2) NULL,
	[reg_pay] [numeric](10, 2) NULL,
	[tmp_pay] [numeric](10, 2) NULL,
	[ovt_pay] [numeric](10, 2) NULL,
	[dbl_OT_Pay] [numeric](10, 2) NULL,
	[ben_pay] [numeric](10, 2) NULL,
	[category_def] [char](20) NULL,
	[pay_rule] [char](25) NULL,
	[pay_rate] [numeric](10, 2) NULL,
	[Head_Count_Code] [char](4) NULL,
	[shift] [char](6) NULL,
	[changed_HCC] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_Pay_Summary] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_RP_Pay_Summary] ADD  CONSTRAINT [DF__a_RP_Pay___LastU__7AF13DF7]  DEFAULT (getdate()) FOR [LastUpdated]