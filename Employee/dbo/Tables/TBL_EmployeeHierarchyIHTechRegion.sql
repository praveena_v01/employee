﻿CREATE TABLE [dbo].[TBL_EmployeeHierarchyIHTechRegion](
	[EnterpriseID] [varchar](10) NOT NULL,
	[NPSID] [varchar](7) NULL,
	[FullName] [varchar](250) NULL,
	[TMEnterpriseID] [varchar](10) NULL,
	[TMFullName] [varchar](250) NULL,
	[DSMEnterpriseID] [varchar](10) NULL,
	[DSMFullName] [varchar](250) NULL,
	[Unit] [char](7) NULL,
	[UnitName] [varchar](250) NULL,
	[TFDEnterpriseID] [varchar](10) NULL,
	[TFDFullName] [varchar](250) NULL,
	[Territory] [char](7) NULL,
	[TerritoryName] [varchar](250) NULL,
	[Region] [char](7) NULL,
	[RegionName] [varchar](250) NULL,
	[RVPEnterpriseID] [varchar](10) NULL,
	[RVPFullName] [varchar](250) NULL,
 CONSTRAINT [PK_TBL_EmployeeHierarchyIHTechRegion] PRIMARY KEY CLUSTERED 
(
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_TBL_EmployeeHierarchyIHTechRegion_Unit]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_EmployeeHierarchyIHTechRegion_Unit] ON [dbo].[TBL_EmployeeHierarchyIHTechRegion]
(
	[NPSID] ASC,
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]