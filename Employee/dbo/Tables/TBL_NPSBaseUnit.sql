﻿CREATE TABLE [dbo].[TBL_NPSBaseUnit](
	[Unit] [char](7) NOT NULL,
	[UnitTypeCode] [char](3) NOT NULL,
	[UnitName] [varchar](27) NOT NULL,
	[UnitStreetAddressLine1] [varchar](30) NOT NULL,
	[UnitStreetAddressLine2] [varchar](30) NOT NULL,
	[UnitCityName] [varchar](20) NOT NULL,
	[UnitStateCode] [char](2) NOT NULL,
	[ZIPCode] [char](5) NOT NULL,
	[ZIPPlus4Code] [char](4) NOT NULL,
	[PartDepot] [char](4) NOT NULL,
	[Field] [char](7) NOT NULL,
	[Region] [char](7) NULL,
	[CentralServiceUnit] [char](7) NOT NULL,
	[AlternateRepairUnit] [char](7) NOT NULL,
	[PATelemarketingPriceLevelCode] [decimal](2, 1) NOT NULL,
	[MarketingUnitInboundTelephone] [char](10) NOT NULL,
	[TaxingGeoCode] [char](9) NOT NULL,
	[BatchUpdateDate] [date] NOT NULL,
	[TaxingCountyCode] [char](3) NOT NULL,
 CONSTRAINT [PK_TBL_NPSBaseUnit] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]