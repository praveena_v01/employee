﻿CREATE TABLE [dbo].[TBL_PeopleSoftPaycheckData](
	[EntryID] [int] IDENTITY(1,1) NOT NULL,
	[AccountingYearWeek] [int] NOT NULL,
	[Region] [char](7) NULL,
	[Unit] [char](7) NULL,
	[FinancialRegion] [char](5) NULL,
	[FinancialUnit] [char](7) NULL,
	[PayGroup] [varchar](7) NULL,
	[DepartmentID] [varchar](12) NULL,
	[EmployeeID] [varchar](12) NULL,
	[EmployeeName] [varchar](50) NULL,
	[EarnCode] [varchar](3) NULL,
	[Earnings] [decimal](16, 4) NULL,
	[Hours] [decimal](16, 4) NULL,
	[JobCode] [varchar](8) NULL,
	[EarnEndDate] [datetime2](7) NULL,
	[PayEndDate] [datetime2](7) NULL,
	[FinancialPayAccount] [varchar](7) NULL,
	[GLDepartment] [varchar](5) NULL,
 CONSTRAINT [PK_TBL_PeopleSoft_PaycheckData] PRIMARY KEY CLUSTERED 
(
	[EntryID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]