﻿CREATE TABLE [dbo].[a_Head_Cnt_Code_Forecast](
	[head_cnt_code] [char](6) NULL,
	[_8961_Goal] [int] NULL,
	[_8963_Goal] [int] NULL,
	[_8966_Goal] [int] NULL,
	[_8976_Goal] [int] NULL,
	[_8842_Goal] [int] NULL,
	[_8961] [int] NULL,
	[_8963] [int] NULL,
	[_8966] [int] NULL,
	[_8976] [int] NULL,
	[_8942] [int] NULL,
	[_8961_cell] [char](4) NULL,
	[_8963_cell] [char](4) NULL,
	[_8966_cell] [char](4) NULL,
	[_8976_cell] [char](4) NULL,
	[_8842_cell] [char](4) NULL,
	[date] [char](10) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Head_Cnt_Code_Forecast] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Head_Cnt_Code_Forecast] ADD  CONSTRAINT [DF__a_Head_Cn__LastU__589C25F3]  DEFAULT (getdate()) FOR [LastUpdated]