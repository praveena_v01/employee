﻿CREATE TABLE [dbo].[TBL_EmployeeHistoricalStatusCode](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[StatusCode] [char](1) NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalStatusCode] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalStatusCode] ADD  DEFAULT (getdate()) FOR [StartDate]