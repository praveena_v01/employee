﻿CREATE TABLE [dbo].[TBL_EmployeeHierarchyIHUnitRegion](
	[Unit] [char](7) NOT NULL,
	[UnitName] [varchar](250) NULL,
	[TFDEnterpriseID] [varchar](10) NULL,
	[TFDFullName] [varchar](250) NULL,
	[Territory] [char](7) NULL,
	[TerritoryName] [varchar](250) NULL,
	[Region] [char](7) NULL,
	[RegionName] [varchar](250) NULL,
	[RVPEnterpriseID] [varchar](10) NULL,
	[RVPFullName] [varchar](250) NULL,
 CONSTRAINT [PK_TBL_EmployeeHierarchyIHUnitRegion] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]