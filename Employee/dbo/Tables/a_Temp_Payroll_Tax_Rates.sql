﻿CREATE TABLE [dbo].[a_Temp_Payroll_Tax_Rates](
	[PK] [int] IDENTITY(1,1) NOT NULL,
	[PDC] [char](5) NULL,
	[Type_Emp] [char](20) NULL,
	[tax_percent] [numeric](6, 2) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
 CONSTRAINT [PK_a_Temp_Payroll_Tax_Rates] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Temp_Payroll_Tax_Rates] ADD  CONSTRAINT [DF_a_Temp_Payroll_Tax_Rates_LastUpdated]  DEFAULT (getdate()) FOR [LastUpdated]