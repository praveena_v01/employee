﻿CREATE TABLE [dbo].[a_Associate_History](
	[ldap_ID] [char](7) NOT NULL,
	[fname] [char](15) NULL,
	[lname] [char](30) NULL,
	[lead_name] [char](15) NULL,
	[rec_RFS] [bit] NULL,
	[rec_CWO] [bit] NULL,
	[ord_CWO] [bit] NULL,
	[ord_TCH] [bit] NULL,
	[default_dept] [char](10) NULL,
	[section] [char](10) NULL,
	[date] [datetime] NOT NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Associate_History] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Associate_History] ADD  CONSTRAINT [DF__a_Associa__LastU__436BFEE3]  DEFAULT (getdate()) FOR [LastUpdated]