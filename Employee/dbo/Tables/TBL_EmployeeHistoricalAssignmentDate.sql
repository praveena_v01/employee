﻿CREATE TABLE [dbo].[TBL_EmployeeHistoricalAssignmentDate](
	[UniqueID] [int] NOT NULL,
	[StartDate] [datetime2](0) NOT NULL,
	[AssignmentDate] [date] NOT NULL,
	[EndDate] [datetime2](0) NULL,
 CONSTRAINT [PK_TBL_EmployeeHistoricalAssignmentDate] PRIMARY KEY CLUSTERED 
(
	[UniqueID] ASC,
	[StartDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_EmployeeHistoricalAssignmentDate] ADD  DEFAULT (getdate()) FOR [StartDate]