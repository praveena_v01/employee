﻿CREATE TABLE [dbo].[a_Default_Dept](
	[Default_Dept] [char](10) NULL,
	[Sub_Dept] [char](10) NULL,
	[PDC] [char](5) NULL,
	[Goal_SubDept] [int] NULL,
	[Goal_Dept] [int] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Default_Dept] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Default_Dept] ADD  CONSTRAINT [DF__a_Default__LastU__4F12BBB9]  DEFAULT (getdate()) FOR [LastUpdated]