﻿CREATE TABLE [dbo].[a_RP_PW_Access](
	[LDAP_ID] [char](8) NULL,
	[Password] [char](25) NULL,
	[pdc] [char](5) NULL,
	[Detail_Access] [bit] NULL,
	[email] [char](35) NULL,
	[send_email] [bit] NULL,
	[JOB] [char](3) NULL,
	[Admin_Rights] [bit] NULL,
	[Missing_FROM_CC_Email] [bit] NULL,
	[Missing_TO_Email] [bit] NULL,
	[Missing_CC_Email] [bit] NULL,
	[Missing_Default_Unit] [char](5) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_RP_PW_Access] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_RP_PW_Access] ADD  CONSTRAINT [DF__a_RP_PW_A__LastU__0662F0A3]  DEFAULT (getdate()) FOR [LastUpdated]