﻿CREATE TABLE [dbo].[Roster](
	[SequenceNumber] [varchar](50) NULL,
	[EmployeeName] [varchar](50) NULL,
	[EmployeeID] [varchar](50) NULL,
	[EnterpriseID] [varchar](50) NULL,
	[JOBCODE] [varchar](50) NULL
) ON [PRIMARY]