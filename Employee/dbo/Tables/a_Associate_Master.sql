﻿CREATE TABLE [dbo].[a_Associate_Master](
	[LDAP_ID] [char](8) NOT NULL,
	[fname] [char](20) NULL,
	[lname] [char](35) NULL,
	[lead_name] [char](15) NULL,
	[default_dept] [char](10) NULL,
	[REC_Default] [char](10) NULL,
	[PAW_Default] [char](10) NULL,
	[ORD_Default] [char](10) NULL,
	[section] [char](10) NULL,
	[REC_CWO] [numeric](5, 2) NULL,
	[REC_RFS] [numeric](5, 2) NULL,
	[REC_VEN] [numeric](5, 2) NULL,
	[PAW_IRC] [numeric](5, 2) NULL,
	[PAW_PDC] [numeric](5, 2) NULL,
	[ORD_TECH] [numeric](5, 2) NULL,
	[ORD_CWO] [numeric](5, 2) NULL,
	[PACKING_TRANS] [numeric](5, 2) NULL,
	[SHIPPING] [numeric](5, 2) NULL,
	[PDC] [char](5) NULL,
	[is_id_in_directory] [char](1) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Associate_Master] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Associate_Master] ADD  CONSTRAINT [DF__a_Associa__LastU__45544755]  DEFAULT (getdate()) FOR [LastUpdated]