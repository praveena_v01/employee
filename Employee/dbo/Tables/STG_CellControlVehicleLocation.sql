﻿CREATE TABLE [dbo].[STG_CellControlVehicleLocation](
	[VehicleDongleMacAddress] [varchar](12) NOT NULL,
	[PhoneNumber] [varchar](15) NULL,
	[LocationTime] [datetime] NOT NULL,
	[ManagerEnterpriseIDKey] [int] NULL,
	[ManagerEnterpriseID] [varchar](11) NULL,
	[ActiveTrip] [bit] NULL,
	[Longitude] [decimal](10, 5) NULL,
	[Latitude] [decimal](10, 5) NULL,
	[Heading] [decimal](10, 5) NULL,
	[Speed] [decimal](10, 5) NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[STG_CellControlVehicleLocation] ADD  CONSTRAINT [DF_STG_CellControlVehicleLocation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]