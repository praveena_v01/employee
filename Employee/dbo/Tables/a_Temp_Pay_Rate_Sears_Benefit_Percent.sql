﻿CREATE TABLE [dbo].[a_Temp_Pay_Rate_Sears_Benefit_Percent](
	[PDC] [char](5) NULL,
	[temp_pay_rate] [decimal](10, 2) NULL,
	[sears_benefit_percent] [decimal](10, 1) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Temp_Pay_Rate_Sears_Benefit_Percent] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Temp_Pay_Rate_Sears_Benefit_Percent] ADD  CONSTRAINT [DF__a_Temp_Pa__LastU__178D7CA5]  DEFAULT (getdate()) FOR [LastUpdated]