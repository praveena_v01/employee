﻿CREATE TABLE [dbo].[a_Lead_Master](
	[LEAD_NAME] [char](15) NOT NULL,
	[PDC] [char](5) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Lead_Master] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Lead_Master] ADD  CONSTRAINT [DF__a_Lead_Ma__LastU__69C6B1F5]  DEFAULT (getdate()) FOR [LastUpdated]