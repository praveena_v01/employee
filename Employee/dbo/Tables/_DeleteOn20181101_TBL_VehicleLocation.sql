﻿CREATE TABLE [dbo].[_DeleteOn20181101_TBL_VehicleLocation](
	[VehicleDongleMacAddress] [varchar](12) NOT NULL,
	[PhoneNumber] [varchar](11) NULL,
	[LocationTime] [datetime] NOT NULL,
	[EnterpriseIDKey] [int] NULL,
	[EnterpriseID] [varchar](11) NULL,
	[ActiveTrip] [bit] NULL,
	[Longitude] [decimal](10, 5) NULL,
	[Latitude] [decimal](10, 5) NULL,
	[Heading] [decimal](10, 5) NULL,
	[Speed] [decimal](10, 5) NULL,
	[InsertDateTime] [datetime] NULL,
	[ModifiedDateTime] [datetime] NULL,
 CONSTRAINT [PK_TBL_VehicleLocation] PRIMARY KEY CLUSTERED 
(
	[VehicleDongleMacAddress] ASC,
	[LocationTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[_DeleteOn20181101_TBL_VehicleLocation] ADD  CONSTRAINT [DF_TBL_VehicleLocation_InsertDateTime]  DEFAULT (getdate()) FOR [InsertDateTime]
GO
/****** Object:  Index [IX_TBL_VehicleLocation_LocationTime_EnterpriseID]    Script Date: 7/15/2019 10:32:27 AM ******/
CREATE NONCLUSTERED INDEX [IX_TBL_VehicleLocation_LocationTime_EnterpriseID] ON [dbo].[_DeleteOn20181101_TBL_VehicleLocation]
(
	[LocationTime] ASC,
	[EnterpriseID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]