﻿CREATE TABLE [dbo].[TBL_RedPrairiePunchDetail](
	[PeopleSoftID] [char](11) NOT NULL,
	[WorkDate] [date] NOT NULL,
	[StartDay] [datetime2](7) NULL,
	[StartPay] [datetime2](7) NULL,
	[LunchStart] [datetime2](7) NULL,
	[LunchEnd] [datetime2](7) NULL,
	[LunchStart2] [datetime2](7) NULL,
	[LunchEnd2] [datetime2](7) NULL,
	[EndPay] [datetime2](7) NULL,
	[EndDay] [datetime2](7) NULL,
	[LastUpdateDate] [datetime2](0) NOT NULL,
 CONSTRAINT [PK_TBL_RedPrairiePunchDetail] PRIMARY KEY NONCLUSTERED 
(
	[PeopleSoftID] ASC,
	[WorkDate] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[TBL_RedPrairiePunchDetail] ADD  DEFAULT (getdate()) FOR [LastUpdateDate]