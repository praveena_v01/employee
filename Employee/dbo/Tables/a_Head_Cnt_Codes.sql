﻿CREATE TABLE [dbo].[a_Head_Cnt_Codes](
	[Head_Cnt_code] [char](6) NULL,
	[Description] [char](32) NULL,
	[HCC_Goal] [numeric](4, 0) NULL,
	[InOutBoundSupt] [char](8) NULL,
	[found] [bit] NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [char](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Head_Cnt_Codes] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Head_Cnt_Codes] ADD  CONSTRAINT [DF__a_Head_Cn__LastU__5E54FF49]  DEFAULT (getdate()) FOR [LastUpdated]