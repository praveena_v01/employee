﻿CREATE TABLE [dbo].[TBL_NPSEmployee](
	[Unit] [varchar](7) NOT NULL,
	[NPSID] [varchar](7) NOT NULL,
	[SocialSecurityID] [varchar](10) NULL,
	[EnterpriseID] [varchar](28) NULL,
	[LastName] [varchar](100) NULL,
	[FirstName] [varchar](100) NULL,
	[MiddleName] [varchar](50) NULL,
	[ClassificationWorkCode] [varchar](1) NULL,
	[ServiceDate] [varchar](30) NULL,
	[TermDate] [varchar](50) NULL,
	[WorkShiftCode] [varchar](2) NULL,
	[AddDeleteCode] [varchar](2) NULL,
	[ManagerEnterpriseID] [varchar](28) NULL,
	[ManagerEmployeeID] [varchar](14) NULL,
	[JobCode] [varchar](10) NULL,
	[HomePhone] [varchar](10) NULL,
	[PagerPhone] [varchar](10) NULL,
	[SecurityCode] [varchar](10) NULL,
	[BranchUnit] [varchar](7) NULL,
 CONSTRAINT [PK_TBL_NPSEmployee] PRIMARY KEY CLUSTERED 
(
	[Unit] ASC,
	[NPSID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]