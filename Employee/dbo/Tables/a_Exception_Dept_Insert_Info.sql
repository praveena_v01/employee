﻿CREATE TABLE [dbo].[a_Exception_Dept_Insert_Info](
	[pdc] [char](5) NULL,
	[ldap_id] [char](8) NULL,
	[department] [char](10) NULL,
	[avg_hrs] [numeric](8, 2) NULL,
	[Table_name] [char](50) NULL,
	[LastUpdated] [datetime] NULL,
	[LastEditedby_LDAP] [nchar](8) NULL,
	[PK] [int] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_a_Exception_Dept_Insert_Info] PRIMARY KEY CLUSTERED 
(
	[PK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[a_Exception_Dept_Insert_Info] ADD  CONSTRAINT [DF__a_Exception__LastU__11D4A34F]  DEFAULT (getdate()) FOR [LastUpdated]