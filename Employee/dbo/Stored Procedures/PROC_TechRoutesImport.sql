﻿-- =============================================
-- Author:		Aaron Cote
-- Create date: 2015-12-10
-- Description:	Imports the current tech routes
-- =============================================
CREATE PROCEDURE [dbo].[PROC_TechRoutesImport]

AS
	BEGIN
	
	-- Merge into primary table
	MERGE 
		Employee.dbo.TBL_TechRoutes AS T
	USING 
		(SELECT
			SISCHED.UnitNumber AS Unit
			,NPSID AS TechID
			,SISCHED.EnterpriseID
			,SISCHED.ServiceOrderNumber AS ServiceOrder
			,SCHEDULE_STATUS AS StatusCode
			,CONTRACT_START AS ScheduleFromTime
			,CONTRACT_COMPLETION AS ScheduleToTime
			,Expected_Start AS EstimatedStart
			,LastSeenDate AS LastUpdate
		FROM 
			TRYDVSQL1.shared_tbls.views.si_schedule_backup SISCHED
		-- Service order may be listed more than once, so make sure latest one
		-- is being selected.
		INNER JOIN
			(SELECT
				UnitNumber
				,ServiceOrderNumber
				,MAX(JOB_NO) AS MaxJob
			FROM
				TRYDVSQL1.shared_tbls.views.si_schedule_backup
			WHERE
				AttemptType = 'REG'
			GROUP BY
				UnitNumber
				,ServiceOrderNumber) SISCHEDMAX
		ON
			SISCHED.UnitNumber = SISCHEDMAX.UnitNumber
			AND
			SISCHED.ServiceOrderNumber = SISCHEDMAX.ServiceOrderNumber
			AND
			SISCHED.JOB_NO = SISCHEDMAX.MaxJob
		INNER JOIN
			(SELECT 
				EnterpriseID
				,CASE 
					WHEN NPSID IS NULL THEN SalesID
					ELSE NPSID
				END AS NPSID
			FROM
				Employee.dbo.view_Employee
			WHERE 
				EnterpriseID <> '') EMP
		ON
			SISCHED.EnterpriseID = EMP.EnterpriseID
		WHERE
			Contract_Start BETWEEN CAST(GETDATE() AS DATE) AND CAST(GETDATE() + 30 AS DATE)
			AND
			SISCHED.EnterpriseID IS NOT NULL
			AND
			AttemptType = 'REG'
			AND
			SISCHED.UnitNumber NOT IN ('0008505', '0008206')) AS S
	ON 
		(T.EnterpriseID = S.EnterpriseID
		AND
		T.ServiceOrder = S.ServiceOrder)
		
	WHEN MATCHED THEN
		UPDATE 
		SET
			T.Unit = S.Unit
			,T.TechID = S.TechID
			,T.StatusCode = S.StatusCode
			,T.ScheduleFromTime = S.ScheduleFromTime
			,T.ScheduleToTime = S.ScheduleToTime
			,T.EstimatedStart = S.EstimatedStart
			,T.LastUpdate = S.LastUpdate
	WHEN NOT MATCHED BY TARGET THEN
		INSERT 
			(Unit
			,TechID
			,EnterpriseID
			,ServiceOrder
			,StatusCode
			,ScheduleFromTime
			,ScheduleToTime
			,EstimatedStart
			,LastUpdate)
		VALUES 
			(S.Unit
			,S.TechID
			,S.EnterpriseID
			,S.ServiceOrder
			,S.StatusCode
			,S.ScheduleFromTime
			,S.ScheduleToTime
			,S.EstimatedStart
			,S.LastUpdate)
	WHEN NOT MATCHED BY SOURCE THEN
		DELETE;
				
END