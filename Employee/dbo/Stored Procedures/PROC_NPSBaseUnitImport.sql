﻿-- ==========================================================================================
-- Author:		Aaron Cote
-- Create date: 08/30/2015
-- Description:	Imports unit data from HS_DAILY_VIEWS.NPSXTBU
-- ==========================================================================================
CREATE PROCEDURE [dbo].[PROC_NPSBaseUnitImport]

AS
	BEGIN
	
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPSBaseUnitTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSBaseUnitTemp;
		END
		
		CREATE TABLE 
			#TBL_NPSBaseUnitTemp
			(Unit [char](7) NOT NULL,
			 UnitTypeCode [char](3) NOT NULL,
			 UnitName [varchar](27) NOT NULL,
			 UnitStreetAddressLine1 [varchar](30) NOT NULL,
			 UnitStreetAddressLine2 [varchar](30) NOT NULL,
			 UnitCityName [varchar](20) NOT NULL,
			 UnitStateCode [char](2) NOT NULL,
			 ZIPCode [char](5) NOT NULL,
			 ZIPPlus4Code [char](4) NOT NULL,
			 PartDepot [char](4) NOT NULL,
			 Field [char](7) NOT NULL,
			 Region [char](7) NULL,
			 CentralServiceUnit [char](7) NOT NULL,
			 AlternateRepairUnit [char](7) NOT NULL,
		     PATelemarketingPriceLevelCode [decimal](2, 1) NOT NULL,
			 MarketingUnitInboundTelephone [char](10) NOT NULL,
			 TaxingGeoCode [char](9) NOT NULL,
			 BatchUpdateDate [date] NOT NULL,
			 TaxingCountyCode [char](3) NOT NULL);
		
		
		SET @querystring = 'INSERT INTO #TBL_NPSBaseUnitTemp (Unit, UnitTypeCode, UnitName, UnitStreetAddressLine1, UnitStreetAddressLine2, UnitCityName, UnitStateCode, ZIPCode, ZIPPlus4Code, PartDepot, Field, Region,CentralServiceUnit, AlternateRepairUnit, PATelemarketingPriceLevelCode, MarketingUnitInboundTelephone, TaxingGeoCode, BatchUpdateDate, TaxingCountyCode)'

		SET @querystring = @querystring + ' SELECT UN_NO, UN_TYP_CD, UN_NM, UN_LN1_AD, UN_LN2_AD, UN_CTY_NM, UN_STE_CD, ZIP_CD, ZIP_SUF_CD, PRT_DPT_NO, FLD_NO, RGN_NO, CTR_SVC_UN_NO, ALT_RPR_UN_NO, MKT_PRC_LVL_CD,MKT_UN_INB_PHN_NO, TAX_GEO_CD, BCH_UPD_DT, TAX_CNY_CD FROM OPENQUERY(TERADATAPROD, '

		SET @querystring = @querystring + ' ''SELECT UN_NO, UN_TYP_CD, UN_NM, UN_LN1_AD, UN_LN2_AD, UN_CTY_NM, UN_STE_CD, ZIP_CD, ZIP_SUF_CD, PRT_DPT_NO, FLD_NO, RGN_NO, CTR_SVC_UN_NO, ALT_RPR_UN_NO, MKT_PRC_LVL_CD,MKT_UN_INB_PHN_NO, TAX_GEO_CD, BCH_UPD_DT, TAX_CNY_CD FROM HS_DAILY_VIEWS.NPSXTBU;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		-- Clean up temp table
		UPDATE 
			#TBL_NPSBaseUnitTemp
		SET
			BatchUpdateDate = '1900-01-01'
		WHERE
			BatchUpdateDate = '0001-01-01';
		
		-- Merge into primary table
		MERGE 
			Employee.dbo.TBL_NPSBaseUnit AS T
		USING 
			(SELECT 
				Unit
				,UnitTypeCode
				,UnitName
				,UnitStreetAddressLine1
				,UnitStreetAddressLine2
				,UnitCityName
				,UnitStateCode
				,ZIPCode
				,ZIPPlus4Code
				,PartDepot
				,Field
				,Region
				,CentralServiceUnit
				,AlternateRepairUnit
				,PATelemarketingPriceLevelCode
				,MarketingUnitInboundTelephone
				,TaxingGeoCode
				,BatchUpdateDate
				,TaxingCountyCode
				,GETDATE() AS LastUpdateTimestamp
			FROM 
				#TBL_NPSBaseUnitTemp) AS S
		ON 
			(T.Unit = S.Unit)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				 T.UnitTypeCode = S.UnitTypeCode
				,T.UnitName = S.UnitName
				,T.UnitStreetAddressLine1 = S.UnitStreetAddressLine1
				,T.UnitStreetAddressLine2 = S.UnitStreetAddressLine2
				,T.UnitCityName = S.UnitCityName
				,T.UnitStateCode = S.UnitStateCode
				,T.ZIPCode = S.ZIPCode
				,T.ZIPPlus4Code = S.ZIPPlus4Code
				,T.PartDepot = S.PartDepot
				,T.Field = S.Field
				,T.CentralServiceUnit = S.CentralServiceUnit
				,T.AlternateRepairUnit = S.AlternateRepairUnit
				,T.PATelemarketingPriceLevelCode = S.PATelemarketingPriceLevelCode
				,T.MarketingUnitInboundTelephone = S.MarketingUnitInboundTelephone
				,T.TaxingGeoCode = S.TaxingGeoCode
				,T.BatchUpdateDate = S.BatchUpdateDate
				,T.TaxingCountyCode = S.TaxingCountyCode
				,T.LastUpdateTimestamp = S.LastUpdateTimestamp			
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Unit
				,UnitTypeCode
				,UnitName
				,UnitStreetAddressLine1
				,UnitStreetAddressLine2
				,UnitCityName
				,UnitStateCode
				,ZIPCode
				,ZIPPlus4Code
				,PartDepot
				,Field
				,CentralServiceUnit
				,AlternateRepairUnit
				,PATelemarketingPriceLevelCode
				,MarketingUnitInboundTelephone
				,TaxingGeoCode
				,BatchUpdateDate
				,TaxingCountyCode
				,LastUpdateTimestamp)
			VALUES 
				(S.Unit
				,S.UnitTypeCode
				,S.UnitName
				,S.UnitStreetAddressLine1
				,S.UnitStreetAddressLine2
				,S.UnitCityName
				,S.UnitStateCode
				,S.ZIPCode
				,S.ZIPPlus4Code
				,S.PartDepot
				,S.Field
				,S.CentralServiceUnit
				,S.AlternateRepairUnit
				,S.PATelemarketingPriceLevelCode
				,S.MarketingUnitInboundTelephone
				,S.TaxingGeoCode
				,S.BatchUpdateDate
				,S.TaxingCountyCode
				,S.LastUpdateTimestamp)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
				
		IF object_id('tempdb..#TBL_NPSBaseUnitTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSBaseUnitTemp;
		END
END