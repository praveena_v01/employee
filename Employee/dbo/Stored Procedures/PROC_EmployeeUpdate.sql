﻿-- =============================================
-- Author:		Aaron Cote
-- Create date: 2015-10-14
-- Description:	Updates the final employee table
-- =============================================
CREATE PROCEDURE [dbo].[PROC_EmployeeUpdate]

AS
	BEGIN

		IF object_id('tempdb..#TBL_EmployeeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_EmployeeTemp;
		END
		
		CREATE TABLE 
			#TBL_EmployeeTemp
			([BusinessUnit] [varchar](4) NOT NULL,
			[FinancialRegion] [varchar](5) NOT NULL,
			[FinancialUnit] [varchar](5) NOT NULL,
			[Region] [varchar](7) NOT NULL,
			[Unit] [varchar](30) NOT NULL,
			[Department] [varchar](31) NOT NULL,
			[SearsDepartment] [varchar](31) NOT NULL,
			[SearsDepartmentName] [varchar](30) NOT NULL,
			[GLDepartment] [varchar](50) NOT NULL,
			[Location] [varchar](5) NOT NULL,
			[LocationDescription] [varchar](30) NOT NULL,
			[PayType] [varchar](3) NOT NULL,
			[PayGroup] [varchar](6) NOT NULL,
			[JobGrade] [varchar](5) NOT NULL,
			[EnterpriseID] [varchar](28) NOT NULL,
			[EmployeeID] [varchar](14) NOT NULL,
			[NPSID] [varchar](7) NOT NULL,
			[SalesID] [varchar](7) NOT NULL,
			[EmployeeStatus] [varchar](1) NOT NULL,
			[EmployeeType] [varchar](1) NOT NULL,
			[LastName] [varchar](100) NOT NULL,
			[FirstName] [varchar](100) NOT NULL,
			[MiddleName] [varchar](50) NOT NULL,
			[FullName] [varchar](250) NOT NULL,
			[DisplayName] [varchar](250) NOT NULL,
			[Email] [varchar](250) NOT NULL,
			[OfficePhone] [varchar](70) NOT NULL,
			[MobilePhone] [varchar](70) NOT NULL,
			[MobilePhoneProvider] [varchar](10) NOT NULL,
			[JobCode] [varchar](10) NOT NULL,
			[JobTitle] [varchar](70) NOT NULL,
			[StartDate] [varchar](10) NOT NULL,
			[JobStartDate] [varchar](10) NOT NULL,
			[TermDate] [varchar](10) NOT NULL,
			[ManagerEnterpriseID] [varchar](28) NOT NULL,
			[ManagerEmployeeID] [varchar](14) NOT NULL,
			[ManagerStartDate] [date] NOT NULL,
			[RowStartDate] [date] NOT NULL);
		
		INSERT INTO
			#TBL_EmployeeTemp
			(BusinessUnit
			,FinancialRegion
			,FinancialUnit
			,Region
			,Unit
			,Department
			,SearsDepartment
			,SearsDepartmentName
			,GLDepartment
			,Location
			,LocationDescription
			,PayType
			,PayGroup
			,JobGrade
			,EnterpriseID
			,EmployeeID
			,NPSID
			,SalesID
			,EmployeeStatus
			,EmployeeType
			,LastName
			,FirstName
			,MiddleName
			,FullName
			,DisplayName
			,Email
			,OfficePhone
			,MobilePhone
			,MobilePhoneProvider
			,JobCode
			,JobTitle
			,StartDate
			,JobStartDate
			,TermDate
			,ManagerEnterpriseID
			,ManagerEmployeeID
			,ManagerStartDate
			,RowStartDate)
		SELECT
			BusinessUnit
			,'' AS FinancialRegion
			,FinancialUnit
			,'' AS Region
			,'00' + FinancialUnit AS Unit
			,Department
			,SearsDepartment
			,SearsDepartmentName
			,'' AS GLDepartment
			,Location
			,LocationDescription
			,PayType
			,PayGroup
			,JobGrade
			,EnterpriseID
			,EmployeeID
			,NPSID
			,SalesID
			,EmployeeStatus
			,EmployeeType
			,LastName
			,FirstName
			,MiddleName
			,FullName
			,DisplayName
			,Email
			,OfficePhone
			,MobilePhone
			,'' AS MobilePhoneProvider
			,JobCode
			,JobTitle
			,StartDate
			,JobStartDate
			,TermDate
			,ManagerEnterpriseID
			,'' AS ManagerEmployeeID
			,CAST(GETDATE() AS DATE) AS ManagerStartDate
			,CAST(GETDATE() AS DATE) AS RowStartDate
		FROM
			Employee.dbo.TBL_LDAPEmployee;
		
		-- DELETE FAKE EMPLOYEE IDS - ADDED ON 10/6/2014
		DELETE FROM 
			#TBL_EmployeeTemp
		WHERE 
			EmployeeID IN ('00000000000', '00000000001')
			OR
			EmployeeID = ''
			OR
			LEN(EmployeeID) < 11;
	
		-- UPDATE FINANCIAL REGION, GLDEPARTMENT FROM PEOPLESOFT
		UPDATE 
			EMPSTAGE
		SET
			FinancialRegion = HREI.FinancialRegion
			,GLDepartment = HREI.GLDepartment
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				EmployeeID
				,ISNULL(FinancialRegion, '') AS FinancialRegion
				,GLDepartment
			FROM 
				Employee.dbo.TBL_PeoplesoftEmployee) HREI
		ON
			EMPSTAGE.EmployeeID = HREI.EmployeeID;
	
		-- UPDATE USING TBL_SSTTechnician(SSTXTPT) DATA
		UPDATE 
			EMPSTAGE
		SET
			Region = SSTPTBU.Region,
			Unit = SSTPTBU.Unit,
			NPSID = SSTPTBU.NPSID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT
				EnterpriseID,
				ISNULL(BU.Region, '') AS Region,
				ISNULL(SSTPT.Unit, '') AS Unit,
				ISNULL(NPSID, '') AS NPSID
			FROM 
				Employee.dbo.TBL_SSTTechnician SSTPT
			INNER JOIN
				(SELECT DISTINCT 
					Unit,
					Region
				FROM 
					Employee.dbo.TBL_NPSBaseUnit) BU
			ON 
				SSTPT.Unit = BU.Unit
			WHERE
				LEN(PendingSoftwareVersion) > 2) SSTPTBU
		ON
			EMPSTAGE.EnterpriseID = SSTPTBU.EnterpriseID;

		-- UPDATE USING TBL_NPSTechnicianPay(NPSXTPY) DATA THAT ONLY HAS EMPLOYEE ID LISTED ONCE
		UPDATE 
			EMPSTAGE
		SET
			Region = PYEIBU.Region,
			Unit = PYEIBU.Unit,
			NPSID = PYEIBU.EmployeeID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT DISTINCT 
				Region,
				PY.Unit,
				NPSID,
				PY.EmployeeID
			  FROM 
				Employee.dbo.TBL_NPSTechnicianPay PY
			INNER JOIN
				(SELECT DISTINCT 
					Unit,
					Region
				FROM 
					Employee.dbo.TBL_NPSBaseUnit) BU
			ON 
				PY.Unit = BU.Unit
			INNER JOIN
				(SELECT 
					EmployeeID
				FROM 
					Employee.dbo.TBL_NPSTechnicianPay
				GROUP BY 
					EmployeeID
				HAVING 
					COUNT(EmployeeID) = 1) PYTWO
			ON 
				PY.EmployeeID = PYTWO.EmployeeID) PYEIBU
		ON
			EMPSTAGE.EmployeeID = PYEIBU.EmployeeID
		WHERE
			EMPSTAGE.Region = '';

		-- UPDATE EMPSTAGE DATA BASED ON EI DATA THAT ONLY HAS ENTERPRISE ID LISTED ONCE

		UPDATE 
			EMPSTAGE
		SET
			Region = EIBU.Region
			,Unit = EIBU.Unit
			,NPSID = EIBU.NPSID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT DISTINCT 
				Region
				,EI.Unit
				,NPSID
				,EI.EnterpriseID
			  FROM 
				Employee.dbo.TBL_NPSEmployee EI
			INNER JOIN 
				(SELECT 
					EnterpriseID
			FROM 
				Employee.dbo.TBL_NPSEmployee
			WHERE 
				EnterpriseID <> ''
				AND
				TermDate = '1111-11-11'
			GROUP BY
				EnterpriseID
			HAVING
				COUNT(EnterpriseID) = 1
				AND
				LEN(EnterpriseID) > 3) EITWO
			ON
				EI.EnterpriseID = EITWO.EnterpriseID
			INNER JOIN
				(SELECT DISTINCT 
					Unit
					,Region
				FROM 
					Employee.dbo.TBL_NPSBaseUnit) BU
			ON 
				EI.Unit = BU.Unit
			WHERE 
				TermDate = '1111-11-11') EIBU
		ON
			EMPSTAGE.EnterpriseID = EIBU.EnterpriseID
		WHERE
			EMPSTAGE.Region = '';

		-- UPDATE EMPSTAGE DATA USING FINANCIAL UNIT CROSS REFERENCE
		--UPDATE 
		--	EMPSTAGE
		--SET
		--	Region = rgn_2013,
		--	Unit = ctr_svc_un_no
		--FROM 
		--	#TBL_EmployeeTemp EMPSTAGE
		--INNER JOIN
		--	(SELECT 
		--		ctr_svc_un_no,
		--		fin_un_no,
		--		CASE WHEN rgn_2013 IS NULL THEN '' ELSE rgn_2013 END AS rgn_2013
		--	FROM 
		--		TRYDVSQL1.shared_tbls.nfdt.Fin_Unit_Xref) XREF
		--ON
		--	EMPSTAGE.PayGroup = XREF.fin_un_no
		--WHERE
		--	EMPSTAGE.Region = '';

		-- UPDATE EMPSTAGE DATA USING FINANCIAL REGION FROM PEOPLESOFT
		UPDATE 
			EMPSTAGE
		SET
			Region = HREI.FinancialRegion
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				'00' + RIGHT(FinancialRegion, 5) AS FinancialRegion,
				EmployeeID
			FROM 
				Employee.dbo.TBL_PeoplesoftEmployee
			WHERE 
				FinancialRegion <> ''
				AND
				FinancialUnit <> '') HREI
		ON
			EMPSTAGE.EmployeeID = HREI.EmployeeID
		WHERE
			EMPSTAGE.Region = '';

		-- UPDATE EMPSTAGE DATA USING TBL_NPSTechnicianPay(NPSXTPY) ON RECORDS THAT HAVE EMPLOYEE LISTED ONLY ONCE PER UNIT
		UPDATE 
			EMPSTAGE
		SET
			Region = PYEIBU.Region,
			NPSID = PYEIBU.NPSID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT DISTINCT 
				ISNULL(Region, '') AS Region,
				PY.Unit,
				ISNULL(EI.NPSID, '') AS NPSID,
				PY.EmployeeID
			  FROM 
				Employee.dbo.TBL_NPSTechnicianPay PY
			INNER JOIN 
				(SELECT 
					Unit,
					NPSID
			  FROM 
				Employee.dbo.TBL_NPSEmployee
			  WHERE 
				EnterpriseID <> ''
				AND
				TermDate = '1111-11-11') EI
			ON
				PY.Unit = EI.Unit
				AND
				PY.NPSID = EI.NPSID
			INNER JOIN
				(SELECT DISTINCT 
					Unit,
					Region
				FROM 
					Employee.dbo.TBL_NPSBaseUnit) BU
			ON 
				PY.Unit = BU.Unit
			INNER JOIN
				(SELECT
					Unit,
					EmployeeID
				FROM 
					Employee.dbo.TBL_NPSTechnicianPay
				GROUP BY 
					Unit,
					EmployeeID
				HAVING 
					COUNT(EmployeeID) = 1) PYTWO
			ON 
				PY.EmployeeID = PYTWO.EmployeeID) PYEIBU
		ON
			EMPSTAGE.EmployeeID = PYEIBU.EmployeeID
			AND
			EMPSTAGE.Unit = PYEIBU.Unit
		WHERE
			EMPSTAGE.Region = '';
			
		-- UPDATE EMPSTAGE UNIT BASED ON EMPSTAGE LOCATIONS 2 TABLE
		UPDATE 
			EMPSTAGE
		SET
			Unit = EMPSTAGELOCTWO.Unit
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				Number,
				'00' + ReportsTo AS Unit
			FROM 
				Employee.dbo.TBL_LDAPLocationsTwo
			  WHERE 
				ReportsTo <> ''
				AND
				ISNUMERIC(ReportsTo) = 1) EMPSTAGELOCTWO
		ON
			EMPSTAGE.PayGroup = EMPSTAGELOCTWO.Number
		WHERE
			EMPSTAGE.Region = '';

		-- UPDATE USING IH TBL_NPSBaseUnit(NPSXTBU) DS Data
		UPDATE 
			EMPSTAGE
		SET
			Region = BU.Region,
			Unit = CentralServiceUnit
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				Unit,
				ISNULL(Region, '') AS Region,
				CentralServiceUnit
			FROM 
				Employee.dbo.TBL_NPSBaseUnit T1
			INNER JOIN
				(SELECT 
					UnitName
					,MAX(BatchUpdateDate) AS BatchUpdateDate
				FROM 
					Employee.dbo.TBL_NPSBaseUnit
				GROUP BY
					UnitName) T2
			ON
				T1.UnitName = T2.UnitName
				AND
				T1.BatchUpdateDate = T2.BatchUpdateDate
			WHERE
				UnitTypeCode = 'DS') BU
		ON
			'00' + EMPSTAGE.PayGroup = BU.Unit
			AND
			EMPSTAGE.Unit <> BU.CentralServiceUnit;

		-- CLEAN UP IN-HOME REGION IDS
		UPDATE 
			#TBL_EmployeeTemp 
		SET
			Region = '0000' + RIGHT(Region, 3)
		WHERE
			Region LIKE '00088%';

		-- UPDATE REMAINING NPS IDS
		UPDATE 
			EMPSTAGE
		SET
			NPSID = EI.NPSID
		  FROM #TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT DISTINCT
				Unit,
				NPSID,
				EnterpriseID
			FROM 
				Employee.dbo.TBL_NPSEmployee
			WHERE 
				EnterpriseID <> '') EI
		ON 
			EMPSTAGE.Unit = EI.Unit
			AND
			EMPSTAGE.EnterpriseID = EI.EnterpriseID
		WHERE
			EMPSTAGE.NPSID = '';

		-- FORMAT DATES
		UPDATE
			#TBL_EmployeeTemp
		SET
			StartDate = SUBSTRING(StartDate, 1, 4) + '-' + SUBSTRING(StartDate, 5, 2) + '-' + SUBSTRING(StartDate, 7, 2)
		WHERE
			LEN(StartDate) = 8;

		UPDATE
			#TBL_EmployeeTemp
		SET
			JobStartDate = SUBSTRING(JobStartDate, 1, 4) + '-' + SUBSTRING(JobStartDate, 5, 2) + '-' + SUBSTRING(JobStartDate, 7, 2)
		WHERE
			LEN(JobStartDate) = 8;
			
		UPDATE
			#TBL_EmployeeTemp
		SET
			TermDate = SUBSTRING(TermDate, 1, 4) + '-' + SUBSTRING(TermDate, 5, 2) + '-' + SUBSTRING(TermDate, 7, 2)
		WHERE
			LEN(TermDate) = 8;

		-- INSERT FAKE EMPLOYEES FROM THE EI TABLE
		INSERT INTO 
			#TBL_EmployeeTemp
			(BusinessUnit
			,FinancialRegion
			,FinancialUnit
			,Region
			,Unit
			,Department
			,SearsDepartment
			,SearsDepartmentName
			,GLDepartment
			,Location
			,LocationDescription
			,PayType
			,PayGroup
			,JobGrade
			,EnterpriseID
			,EmployeeID
			,NPSID
			,SalesID
			,EmployeeStatus
			,EmployeeType
			,LastName
			,FirstName
			,MiddleName
			,FullName
			,DisplayName
			,Email
			,OfficePhone
			,MobilePhone
			,MobilePhoneProvider
			,JobCode
			,JobTitle
			,StartDate
			,JobStartDate
			,TermDate
			,ManagerEnterpriseID
			,ManagerEmployeeID
			,ManagerStartDate
			,RowStartDate)
		SELECT DISTINCT
			'' AS BusinessUnit
			,'' AS FinancialRegion
			,'' AS FinancialUnit
			,ISNULL(Region, '') AS Region
			,ISNULL(EI.Unit, '') AS Unit
			,'' AS Department
			,'' AS SearsDepartment
			,'' AS SearsDepartmentName
			,'' AS GLDepartment
			,'' AS Location
			,'' AS LocationDescription
			,'' AS PayType
			,'' AS PayGroup
			,'' AS JobGrade
			,'' AS EnterpriseID
			,ISNULL(EI.Unit, '') + ISNULL(NPSID, '') AS EmployeeID
			,ISNULL(NPSID, '')
			,'' AS SalesID
			,'' AS EmployeeStatus
			,'' AS EmployeeType
			,LTRIM(RTRIM(LastName)) AS LastName
			,LTRIM(RTRIM(FirstName)) AS FirstName
			,LTRIM(RTRIM(MiddleName)) AS MiddleName
			,LTRIM(RTRIM(FirstName)) + ' ' + LTRIM(RTRIM(MiddleName)) + ' ' + LTRIM(RTRIM(LastName)) AS FullName
			,LTRIM(RTRIM(LastName)) + ', ' + LTRIM(RTRIM(FirstName)) AS DisplayName
			,'' AS Email
			,'' AS OfficePhone
			,'' AS MobilePhone
			,'' AS MobilePhoneProvider
			,'' AS JobCode
			,'' AS JobTitle
			,ServiceDate AS StartDate
			,ServiceDate AS JobStartDate
			,'' AS TermDate
			,CASE WHEN SUPEI.ManagerEnterpriseID IS NULL THEN '' ELSE LTRIM(RTRIM(SUPEI.ManagerEnterpriseID)) END AS ManagerEnterpriseID
			,'' AS ManagerEmployeeID
			,CAST(GETDATE() AS DATE) AS ManagerStartDate
			,CAST(GETDATE() AS DATE) AS RowStartDate
		  FROM 
			Employee.dbo.TBL_NPSEmployee EI
		LEFT JOIN
			(SELECT
				Unit AS ManagerUnit,
				NPSID AS ManagerEmployeeID,
				EI.EnterpriseID AS ManagerEnterpriseID
			FROM 
				Employee.dbo.TBL_NPSEmployee EI
			INNER JOIN
				(SELECT
					EnterpriseID
				FROM 
					#TBL_EmployeeTemp) EMPSTAGE
			ON 
				EMPSTAGE.EnterpriseID = EI.EnterpriseID
			WHERE
				TermDate = '1111-11-11'
				AND
				EI.EnterpriseID <> ''
				AND
				LEN(EI.EnterpriseID) > 3) SUPEI
		ON
			EI.Unit = SUPEI.ManagerUnit
			AND
			EI.ManagerEmployeeID = SUPEI.ManagerEmployeeID
		LEFT JOIN 
			(SELECT 
				Unit,
				Region
			FROM 
				Employee.dbo.TBL_NPSBaseUnit
			WHERE Region <> '') BU
		ON 
			EI.Unit = BU.Unit
		WHERE 
			TermDate = '1111-11-11'
			AND
			EnterpriseID NOT IN (SELECT EnterpriseID FROM #TBL_EmployeeTemp WHERE EnterpriseID <> '')
			AND
			EI.Unit + NPSID NOT IN (SELECT Unit + NPSID FROM #TBL_EmployeeTemp WHERE Unit <> '' AND NPSID <> '');

		-- UPDATE REMAING RECORDS WITH THE REGION
		UPDATE 
			EMPSTAGE 
		SET
			Region = BU.Region
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN 
			(SELECT 
				Unit,
				Region
			FROM 
				Employee.dbo.TBL_NPSBaseUnit
			WHERE Region <> '') BU
		ON 
			EMPSTAGE.Unit = BU.Unit;
		--WHERE
		--	EMPSTAGE.Region = ''
		--	OR
		--	(SUBSTRING(Region,4,1) <> '8'
		--	AND
		--	SUBSTRING(Region,4,1) <> '0');

		-- Clean up routine for regions and units using the
		-- region and unit that show up the maximum number
		-- of times for a specific pay group.
		UPDATE 
			EMPSTAGE
		SET 
			Region = STAGE.Region
			,Unit = STAGE.Unit
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN 
			(SELECT 
				Region
				,Unit
				,EMPFINAL.PayGroup
			FROM 
				#TBL_EmployeeTemp EMPFINAL
			INNER JOIN
				(SELECT 
					EmployeeStaging.PayGroup
					,MAX(PayGroupCountSource) AS PayGroupCount
				FROM 
					#TBL_EmployeeTemp EmployeeStaging
				INNER JOIN
					(SELECT 
						Region
						,Unit
						,PayGroup
						,COUNT(PayGroup) AS PayGroupCountSource
					FROM 
						#TBL_EmployeeTemp
					WHERE 
						PayGroup IS NOT NULL 
						AND 
						PayGroup <> ''
					GROUP BY 
						Region
						,Unit
						,PayGroup) EmployeeStagingTWO
				ON 
					EmployeeStaging.Region = EmployeeStagingTWO.Region
					AND
					EmployeeStaging.Unit = EmployeeStagingTWO.Unit
					AND
					EmployeeStaging.PayGroup = EmployeeStagingTWO.PayGroup
				GROUP BY
					EmployeeStaging.PayGroup) EmployeeStaging
			ON EMPFINAL.PayGroup = EmployeeStaging.PayGroup
			GROUP BY 
				Region
				,Unit
				,EMPFINAL.PayGroup
				,PayGroupCount
			HAVING
				COUNT(EMPFINAL.PayGroup) = PayGroupCount) STAGE
		ON EMPSTAGE.PayGroup = STAGE.PayGroup

		-- UPDATE REGION EMPSTAGE DATA USING FINANCIAL REGION NUMBER INFORMATION FROM PEOPLESOFT
		UPDATE 
			EMPSTAGE
		SET
			Region = FINANCE.FinancialRegion
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT 
				'0000' + RIGHT(FinancialRegion, 3) AS FinancialRegion
				,'000' + RIGHT(FinancialUnit, 4) AS FinancialUnit
				,EmployeeID
			FROM 
				Employee.dbo.TBL_PeoplesoftEmployee
			WHERE 
				FinancialRegion <> ''
				AND
				FinancialRegion IN ('58820', '58830', '58890', '58900')) FINANCE
		ON
			EMPSTAGE.EmployeeID = FINANCE.EmployeeID;

		-- UPDATE ROUTING EMPLOYEES
		UPDATE
			#TBL_EmployeeTemp
		SET
			Unit = '00' + PayGroup,
			NPSID = ''
		WHERE
			PayGroup IN ('08251', '08261', '08253', '09302');
			
		-- UPDATE ROUTING EMPLOYEES PART 2
		UPDATE
			#TBL_EmployeeTemp
		SET
			Unit = '0008253',
			NPSID = ''
		WHERE
			PayGroup = '04265';

		-- UPDATE CARRY-IN EMPLOYEES
		UPDATE
			#TBL_EmployeeTemp
		SET
			Region = '0000490',
			NPSID = ''
		WHERE
			PayGroup = '24409';
			
		-- UPDATE CORPORATE EMPLOYEES
		--UPDATE
		--	#TBL_EmployeeTemp
		--SET
		--	Region = '0000490'
		--WHERE
		--	Paygroup = '58490';


		---- ADD MANAGER ENTERPRISE IDS FROM EI FOR EMPLOYEES THAT ARE MISSING THE INFORMATION

		--UPDATE
		--	LDAP 
		--SET
		--	ManagerEnterpriseID = mgr_rac_id_no
		--FROM 
		--	#TBL_EmployeeTemp EMPSTAGE
		--INNER JOIN
		--	(SELECT 
		--		emp_rac_id_no,
		--		mgr_rac_id_no
		--	FROM 
		--		shared_tbls.td.npsxtei EI
		--	INNER JOIN
		--		(SELECT 
		--		svc_un_no,
		--		emp_id_no,
		--		LOWER(emp_rac_id_no) AS mgr_rac_id_no
		--	FROM 
		--		shared_tbls.td.npsxtei
		--	WHERE 
		--		emp_rac_id_no IN (SELECT EnterpriseID FROM #TBL_EmployeeTemp WHERE EnterpriseID <> '')) MGREI
		--	ON
		--		EI.svc_un_no = MGREI.svc_un_no
		--		AND
		--		EI.spr_emp_id_1_no = MGREI.emp_id_no
		--	WHERE
		--		emp_rac_id_no <> '') EISOURCE
		--ON 
		--	EMPSTAGE.EnterpriseID = EISOURCE.emp_rac_id_no
		--WHERE 
		--	ManagerEnterpriseID = ''
		--	AND
		--	Unit <> ''
		--	AND
		--	PayGroup <> '';

		-- ADD MANAGER EMPLOYEE IDS
		UPDATE
			EMPSTAGE 
		SET
			ManagerEmployeeID = MGREmployeeID
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT
				EnterpriseID AS MGREnterpriseID,
				EmployeeID AS MGREmployeeID
			FROM
				#TBL_EmployeeTemp
			WHERE 
				EmployeeID <> '') MGREMPSTAGE
		ON 
			EMPSTAGE.ManagerEnterpriseID = MGREMPSTAGE.MGREnterpriseID
		WHERE 
			ManagerEnterpriseID <> '';
			
		---- UPDATE JOB START DATE TO MAKE SURE IT STAYS
		---- CONSISTENT WITH THE JOB CODE, EVEN IF THERE'S A CHANGE IN THE UNIT
		---- THAT WOULD CAUSE THE START DATE TO CHANGE

		--UPDATE
		--	LDAP 
		--SET
		--	JobStartDate = JC.JobStartDate
		--FROM 
		--	#TBL_EmployeeTemp EMPSTAGE
		--INNER JOIN
		--	(SELECT 
		--		EmployeeID, 
		--		JobCode, 
		--		JobStartDate 
		--	FROM 
		--		shared_tbls.dbo.view_Employee) JC
		--ON 
		--	EMPSTAGE.EmployeeID = JC.EmployeeID
		--	AND
		--	EMPSTAGE.JobCode = JC.JobCode
		--WHERE 
		--	EMPSTAGE.JobCode <> '';
			
		-- UPDATE TECH CELL PHONE NUMBERS AND PROVIDERS FROM TPMS
		UPDATE
			EMPSTAGE 
		SET
			MobilePhone = TPMSPHONE.MobilePhone
			,MobilePhoneProvider = TPMSPHONE.MobilePhoneProvider
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT
				EnterpriseID
				,MobilePhoneNumber AS MobilePhone
				,CASE 
					WHEN EmailAddress LIKE '%sprint%' THEN 'SPRINT'
					WHEN EmailAddress LIKE '%vzw%' OR EmailAddress LIKE '%vtext%' THEN 'VERIZON'
					WHEN EmailAddress LIKE '%att%' THEN 'ATT'
					WHEN EmailAddress LIKE '%tmo%' THEN 'TMOBILE'
					WHEN (EmailAddress NOT LIKE '%sprint%' AND EmailAddress NOT LIKE '%att%' AND EmailAddress NOT LIKE '%vtext%' AND EmailAddress NOT LIKE '%vzw%' AND EmailAddress NOT LIKE '%tmo%') OR EmailAddress IS NULL THEN ''
				END AS MobilePhoneProvider
			FROM
				Employee.dbo.TBL_AIMEmployee
			WHERE 
				MobilePhoneNumber <> ''
				AND
				MobilePhoneNumber IS NOT NULL) TPMSPHONE
		ON 
			EMPSTAGE.EnterpriseID = TPMSPHONE.EnterpriseID;
			
		-- UPDATE OFFICE PHONE NUMBER FORMAT
		UPDATE
			EMPSTAGE 
		SET
			OfficePhone = ISNULL(U.clean, '')
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		CROSS APPLY
		(
			SELECT 
				SUBSTRING(OfficePhone, v.number,1)
			FROM 
				master..spt_values v
			WHERE 
				v.type='P' 
				AND 
				v.number BETWEEN 1 AND LEN(OfficePhone)
				AND 
				SUBSTRING(OfficePhone, v.number,1) LIKE '[0-9]'
			ORDER BY 
				v.number
			FOR XML PATH ('')
		) U(clean)
		WHERE 
			OfficePhone <> '';

		-- UPDATE MOBILE PHONE NUMBER FORMAT
		UPDATE
			EMPSTAGE 
		SET
			MobilePhone = ISNULL(U.clean, '')
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		CROSS APPLY
		(
			SELECT 
				SUBSTRING(MobilePhone, v.number,1)
			FROM 
				master..spt_values v
			WHERE 
				v.type='P' 
				AND 
				v.number BETWEEN 1 AND LEN(MobilePhone)
				AND 
				SUBSTRING(MobilePhone, v.number,1) LIKE '[0-9]'
			ORDER BY 
				v.number
			FOR XML PATH ('')
		) U(clean)
		WHERE 
			MobilePhone <> '';
		
		-- UPDATE START DATES
		UPDATE
			EMPSTAGE 
		SET
			ManagerStartDate = EMP.ManagerStartDate
			,RowStartDate = EMP.RowStartDate
		FROM 
			#TBL_EmployeeTemp EMPSTAGE
		INNER JOIN
			(SELECT
				EmployeeID
				,ManagerStartDate
				,RowStartDate
			FROM
				Employee.dbo.TBL_Employee) EMP
		ON 
			EMPSTAGE.EmployeeID = EMP.EmployeeID;
		
		-- MERGE DATA
		MERGE 
			Employee.dbo.TBL_Employee AS T
		USING 
			(SELECT
				BusinessUnit
				,FinancialRegion
				,FinancialUnit
				,Region
				,Unit
				,Department
				,SearsDepartment
				,SearsDepartmentName
				,GLDepartment
				,Location
				,LocationDescription
				,PayType
				,PayGroup
				,JobGrade
				,EnterpriseID
				,EmployeeID
				,NPSID
				,SalesID
				,EmployeeStatus
				,EmployeeType
				,LastName
				,FirstName
				,MiddleName
				,FullName
				,DisplayName
				,Email
				,OfficePhone
				,MobilePhone
				,MobilePhoneProvider
				,JobCode
				,JobTitle
				,StartDate
				,JobStartDate
				,TermDate
				,ManagerEnterpriseID
				,ManagerEmployeeID
				,ManagerStartDate
				,RowStartDate
			FROM 
				#TBL_EmployeeTemp) AS S
		ON 
			(T.BusinessUnit = S.BusinessUnit
			AND
			T.FinancialRegion = S.FinancialRegion
			AND
			T.FinancialUnit = S.FinancialUnit
			AND
			T.Region = S.Region
			AND
			T.Unit = S.Unit
			AND
			T.Department = S.Department
			AND
			T.SearsDepartment = S.SearsDepartment
			AND
			T.SearsDepartmentName = S.SearsDepartmentName
			AND
			T.GLDepartment = S.GLDepartment
			AND
			T.Location = S.Location
			AND
			T.LocationDescription = S.LocationDescription
			AND
			T.PayType = S.PayType
			AND
			T.PayGroup = S.PayGroup
			AND
			T.JobGrade = S.JobGrade
			AND
			T.EnterpriseID = S.EnterpriseID
			AND
			T.EmployeeID = S.EmployeeID
			AND
			T.NPSID = S.NPSID
			AND
			T.SalesID = S.SalesID
			AND
			T.EmployeeStatus = S.EmployeeStatus
			AND
			T.EmployeeType = S.EmployeeType
			AND
			T.LastName = S.LastName
			AND
			T.FirstName = S.FirstName
			AND
			T.MiddleName = S.MiddleName
			AND
			T.FullName = S.FullName
			AND
			T.DisplayName = S.DisplayName
			AND
			T.Email = S.Email
			AND
			T.OfficePhone = S.OfficePhone
			AND
			T.MobilePhone = S.MobilePhone
			AND
			T.MobilePhoneProvider = S.MobilePhoneProvider
			AND
			T.JobCode = S.JobCode
			AND
			T.JobTitle = S.JobTitle
			AND
			T.StartDate = S.StartDate
			AND
			T.JobStartDate = S.JobStartDate
			AND
			T.TermDate = S.TermDate
			AND
			T.ManagerEnterpriseID = S.ManagerEnterpriseID
			AND
			T.ManagerEmployeeID = S.ManagerEmployeeID)
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(UniqueID
				,BusinessUnit
				,FinancialRegion
				,FinancialUnit
				,Region
				,Unit
				,Department
				,SearsDepartment
				,SearsDepartmentName
				,GLDepartment
				,Location
				,LocationDescription
				,PayType
				,PayGroup
				,JobGrade
				,EnterpriseID
				,EmployeeID
				,NPSID
				,SalesID
				,EmployeeStatus
				,EmployeeType
				,LastName
				,FirstName
				,MiddleName
				,FullName
				,DisplayName
				,Email
				,OfficePhone
				,MobilePhone
				,MobilePhoneProvider
				,JobCode
				,JobTitle
				,StartDate
				,JobStartDate
				,TermDate
				,ManagerEnterpriseID
				,ManagerEmployeeID
				,ManagerStartDate
				,RowStartDate)
			VALUES
				(NEWID()
				,S.BusinessUnit
				,S.FinancialRegion
				,S.FinancialUnit
				,S.Region
				,S.Unit
				,S.Department
				,S.SearsDepartment
				,S.SearsDepartmentName
				,S.GLDepartment
				,S.Location
				,S.LocationDescription
				,S.PayType
				,S.PayGroup
				,S.JobGrade
				,S.EnterpriseID
				,S.EmployeeID
				,S.NPSID
				,S.SalesID
				,S.EmployeeStatus
				,S.EmployeeType
				,S.LastName
				,S.FirstName
				,S.MiddleName
				,S.FullName
				,S.DisplayName
				,S.Email
				,S.OfficePhone
				,S.MobilePhone
				,S.MobilePhoneProvider
				,S.JobCode
				,S.JobTitle
				,S.StartDate
				,S.JobStartDate
				,S.TermDate
				,S.ManagerEnterpriseID
				,S.ManagerEmployeeID
				,S.ManagerStartDate
				,S.RowStartDate)
		WHEN NOT MATCHED BY SOURCE THEN
			UPDATE 
			SET 
				T.RowEndDate = GETDATE() - 1;
		
		IF object_id('tempdb..#TBL_EmployeeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_EmployeeTemp;
		END
		
	END