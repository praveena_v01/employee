﻿-- =============================================
-- Author:		Aaron Cote
-- Create date: 8/30/2015
-- Description: Imports the NPS Employee data
-- =============================================
CREATE PROCEDURE [dbo].[PROC_NPSEmployeeImport]

AS
	BEGIN
	
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPSEmployeeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSEmployeeTemp;
		END
		
		CREATE TABLE 
			#TBL_NPSEmployeeTemp
			(Unit [varchar] (7) NOT NULL,
			NPSID [varchar](7) NOT NULL,
			SocialSecurityID [varchar](10) NULL,
			EnterpriseID [varchar](28) NULL,
			LastName [varchar](100) NULL,
			FirstName [varchar](100) NULL,
			MiddleName [varchar](50) NULL,
			ClassificationWorkCode [varchar](1) NULL,
			ServiceDate [varchar](30) NULL,
			TermDate [varchar](50) NULL,
			WorkShiftCode [varchar](2) NULL,
			AddDeleteCode [varchar](2) NULL,
			ManagerEnterpriseID [varchar](28) NULL,
			ManagerEmployeeID [varchar](14) NULL,
			JobCode [varchar](10) NULL,
			HomePhone [varchar](10) NULL,
			PagerPhone [varchar](10) NULL,
			SecurityCode [varchar](10) NULL,
			BranchUnit [varchar](7) NULL);
		
		
		SET @querystring = 'INSERT INTO #TBL_NPSEmployeeTemp ([Unit],[NPSID],[SocialSecurityID],[EnterpriseID],[LastName],[FirstName],[MiddleName],[ClassificationWorkCode],[ServiceDate],[TermDate],[WorkShiftCode],[AddDeleteCode],[ManagerEnterpriseID],[ManagerEmployeeID],[JobCode],[HomePhone],[PagerPhone],[SecurityCode],[BranchUnit])'

		SET @querystring = @querystring + ' SELECT svc_un_no, emp_id_no ,emp_fic_no ,emp_rac_id_no ,emp_lst_nm ,emp_fst_nm ,emp_mid_inl_nm ,emp_clf_wrk_cd ,emp_svc_dt ,emp_trm_dt ,wrk_shf_cd ,emp_add_del_cd ,spr_emp_id_1_no ,spr_emp_id_2_no ,job_skl_cd ,emp_hme_phn_no ,emp_pgr_phn_no ,emp_sec_cd ,br_svc_un_no FROM OPENQUERY(TERADATAPROD, '

		SET @querystring = @querystring + ' ''SELECT svc_un_no, emp_id_no ,emp_fic_no ,emp_rac_id_no ,emp_lst_nm ,emp_fst_nm ,emp_mid_inl_nm ,emp_clf_wrk_cd ,emp_svc_dt ,emp_trm_dt ,wrk_shf_cd ,emp_add_del_cd ,spr_emp_id_1_no ,spr_emp_id_2_no ,job_skl_cd ,emp_hme_phn_no ,emp_pgr_phn_no ,emp_sec_cd ,br_svc_un_no FROM HS_DAILY_VIEWS.NPSXTEI;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		
		
		-- Merge into primary table
		MERGE 
			Employee.dbo.TBL_NPSEmployee AS T
		USING 
			(SELECT 
			   Unit
			  ,NPSID
			  ,SocialSecurityID
			  ,EnterpriseID
			  ,LastName
			  ,FirstName
			  ,MiddleName
			  ,ClassificationWorkCode
			  ,ServiceDate
			  ,TermDate
			  ,WorkShiftCode
			  ,AddDeleteCode
			  ,ManagerEnterpriseID
			  ,ManagerEmployeeID
			  ,JobCode
			  ,HomePhone
			  ,PagerPhone
			  ,SecurityCode
			  ,BranchUnit
			FROM 
				#TBL_NPSEmployeeTemp) AS S
		ON 
			(T.Unit = S.Unit 
			AND
			T.NPSID = S.NPSID)
			
		WHEN MATCHED THEN
			UPDATE 
			SET
				T.SocialSecurityID = S.SocialSecurityID
				,T.EnterpriseID = S.EnterpriseID
				,T.LastName = S.LastName
				,T.FirstName = S.FirstName
				,T.MiddleName = S.MiddleName
				,T.ClassificationWorkCode = S.ClassificationWorkCode
				,T.ServiceDate = S.ServiceDate
				,T.TermDate = S.TermDate
				,T.WorkShiftCode = S.WorkShiftCode
				,T.AddDeleteCode = S.AddDeleteCode
				,T.ManagerEnterpriseID = S.ManagerEnterpriseID
				,T.ManagerEmployeeID = S.ManagerEmployeeID
				,T.JobCode = S.JobCode
				,T.HomePhone = S.HomePhone
				,T.PagerPhone = S.PagerPhone
				,T.SecurityCode = S.SecurityCode
				,T.BranchUnit = S.BranchUnit
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Unit
				,NPSID
				,SocialSecurityID
				,EnterpriseID
				,LastName
				,FirstName
				,MiddleName
				,ClassificationWorkCode
				,ServiceDate
				,TermDate
				,WorkShiftCode
				,AddDeleteCode
				,ManagerEnterpriseID
				,ManagerEmployeeID
				,JobCode
				,HomePhone
				,PagerPhone
				,SecurityCode
				,BranchUnit)
			VALUES 
				(S.Unit
				,S.NPSID
				,S.SocialSecurityID
				,S.EnterpriseID
				,S.LastName
				,S.FirstName
				,S.MiddleName
				,S.ClassificationWorkCode
				,S.ServiceDate
				,S.TermDate
				,S.WorkShiftCode
				,S.AddDeleteCode
				,S.ManagerEnterpriseID
				,S.ManagerEmployeeID
				,S.JobCode
				,S.HomePhone
				,S.PagerPhone
				,S.SecurityCode
				,S.BranchUnit);
				
		IF object_id('tempdb..#TBL_NPSEmployeeTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSEmployeeTemp;
		END
END