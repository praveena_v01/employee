﻿-- =============================================
-- Author:		Aaron Cote
-- Create date: 2015-10-14
-- Description:	Imports the SST technician data
-- =============================================
CREATE PROCEDURE [dbo].[PROC_SSTTechnicianImport]

AS
	BEGIN
	
	-- Merge into primary table
	MERGE 
		Employee.dbo.TBL_SSTTechnician AS T
	USING 
		(SELECT 
			USR_ID
			,PT_IP_AD_TX
			,CUR_SFW_VRS_ID_NO
			,PND_SFW_VRS_ID_NO
			,SVC_UN_NO
			,EMP_ID_NO
			,SSN_NO
			,SU_DAT_DWD_FL
			,TM_ZN_FAC_CD
			,SND_AUT_SYN_FL
			,WLS_SYN_FL
			,HAL_FL
			,ULO_LOG_DT
			,CSAT_FL
			,SYWR_FL
			,LMC_FL
			,EMR_ORD_FL
			,AIM_FL
			,PR_LOTTO_FL
			,EMPLID
			,LOCN
			,LMCDMZ_FL
			,LMCTCP_FL
			,NEWSST_FL
			,TI_FL
		FROM
			TRYDVSQL1.shared_tbls.sst.SSTXTPT) AS S
	ON 
		(T.EnterpriseID = S.USR_ID)
		
	WHEN MATCHED THEN
		UPDATE 
		SET
			T.Unit = S.SVC_UN_NO
			,T.Location = S.LOCN
			,T.EnterpriseID = S.USR_ID
			,T.EmployeeID = S.EMPLID
			,T.NPSID = S.EMP_ID_NO
			,T.IPAddress = S.PT_IP_AD_TX
			,T.CurrentSoftwareVersion = S.CUR_SFW_VRS_ID_NO
			,T.PendingSoftwareVersion = S.PND_SFW_VRS_ID_NO
			,T.SocialSecurityNumber = S.SSN_NO
			,T.SupportDataDownloaded = S.SU_DAT_DWD_FL
			,T.TimeZoneFactor = S.TM_ZN_FAC_CD
			,T.SendAutoSyn = S.SND_AUT_SYN_FL
			,T.WLSSyn = S.WLS_SYN_FL
			,T.HAL = S.HAL_FL
			,T.ULOLogDate = S.ULO_LOG_DT
			,T.CSAT = S.CSAT_FL
			,T.SYWR = S.SYWR_FL
			,T.LMC = S.LMC_FL
			,T.EMROORD = S.EMR_ORD_FL
			,T.AIM = S.AIM_FL
			,T.PRLotto = S.PR_LOTTO_FL
			,T.LMCDMZ = S.LMCDMZ_FL
			,T.LMCTCP = S.LMCTCP_FL
			,T.NewSST = S.NEWSST_FL
			,T.TI = S.TI_FL	
	WHEN NOT MATCHED BY TARGET THEN
		INSERT 
			(Unit
			,Location
			,EnterpriseID
			,EmployeeID
			,NPSID
			,IPAddress
			,CurrentSoftwareVersion
			,PendingSoftwareVersion
			,SocialSecurityNumber
			,SupportDataDownloaded
			,TimeZoneFactor
			,SendAutoSyn
			,WLSSyn
			,HAL
			,ULOLogDate
			,CSAT
			,SYWR
			,LMC
			,EMROORD
			,AIM
			,PRLotto
			,LMCDMZ
			,LMCTCP
			,NewSST
			,TI)
		VALUES 
			(S.SVC_UN_NO
			,S.LOCN
			,S.USR_ID
			,S.EMPLID
			,S.EMP_ID_NO
			,S.PT_IP_AD_TX
			,S.CUR_SFW_VRS_ID_NO
			,S.PND_SFW_VRS_ID_NO
			,S.SSN_NO
			,S.SU_DAT_DWD_FL
			,S.TM_ZN_FAC_CD
			,S.SND_AUT_SYN_FL
			,S.WLS_SYN_FL
			,S.HAL_FL
			,S.ULO_LOG_DT
			,S.CSAT_FL
			,S.SYWR_FL
			,S.LMC_FL
			,S.EMR_ORD_FL
			,S.AIM_FL
			,S.PR_LOTTO_FL
			,S.LMCDMZ_FL
			,S.LMCTCP_FL
			,S.NEWSST_FL
			,S.TI_FL);
				
END