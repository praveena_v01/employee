﻿-- =============================================
-- Author:		Aaron Cote
-- Create date: 2015-10-19
-- Description:	Updates the employee hierarchy tables
-- =============================================
CREATE PROCEDURE [dbo].[PROC_EmployeeHierarchyUpdate]

AS
	BEGIN
		
		-- UPDATE GENERIC HIERARCHY
		IF object_id('tempdb..#TBL_EmployeeHierarchyTemp') IS NOT NULL
		BEGIN
			DROP TABLE TBL_EmployeeHierarchyTemp;
		END
		
		CREATE TABLE 
			#TBL_EmployeeHierarchyTemp
			([EnterpriseID] [varchar](28) NOT NULL,
			[FullName] [varchar](250) NULL,
			[ManagerEnterpriseID] [varchar](28) NULL,
			[ManagerFullName] [varchar](250) NULL,
			[ManagerJobCode] [varchar](10) NULL,
			[ManagerJobTitle] [varchar](70) NULL,
			[ManagerTwoEnterpriseID] [varchar](28) NULL,
			[ManagerTwoFullName] [varchar](250) NULL,
			[ManagerTwoJobCode] [varchar](10) NULL,
			[ManagerTwoJobTitle] [varchar](70) NULL,
			[ManagerThreeEnterpriseID] [varchar](28) NULL,
			[ManagerThreeFullName] [varchar](250) NULL,
			[ManagerThreeJobCode] [varchar](10) NULL,
			[ManagerThreeJobTitle] [varchar](70) NULL,
			[ManagerFourEnterpriseID] [varchar](28) NULL,
			[ManagerFourFullName] [varchar](250) NULL,
			[ManagerFourJobCode] [varchar](10) NULL,
			[ManagerFourJobTitle] [varchar](70) NULL,
			[ManagerFiveEnterpriseID] [varchar](28) NULL,
			[ManagerFiveFullName] [varchar](250) NULL,
			[ManagerFiveJobCode] [varchar](10) NULL,
			[ManagerFiveJobTitle] [varchar](70) NULL);
		
		INSERT INTO
			#TBL_EmployeeHierarchyTemp
			(EnterpriseID
			,FullName
			,ManagerEnterpriseID)
		SELECT
			EnterpriseID
			,FullName
			,ManagerEnterpriseID
		FROM
			Employee.dbo.TBL_Employee WITH (NOLOCK)
		WHERE
			RowEndDate IS NULL
			AND
			EnterpriseID <> '';
			
		-- UPDATE MANAGER LEVEL 1
		UPDATE 
			EMPHIER
		SET
			ManagerFullName = EMP.FullName
			,ManagerJobCode = EMP.JobCode
			,ManagerJobTitle = EMP.JobTitle
			,ManagerTwoEnterpriseID = EMP.ManagerEnterpriseID
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
				,ManagerEnterpriseID
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerEnterpriseID <> ''
			AND
			EMPHIER.ManagerEnterpriseID IS NOT NULL;
			
		-- UPDATE MANAGER LEVEL 2
		UPDATE 
			EMPHIER
		SET
			ManagerTwoFullName = EMP.FullName
			,ManagerTwoJobCode = EMP.JobCode
			,ManagerTwoJobTitle = EMP.JobTitle
			,ManagerThreeEnterpriseID = EMP.ManagerEnterpriseID
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
				,ManagerEnterpriseID
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerTwoEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerTwoEnterpriseID <> ''
			AND
			EMPHIER.ManagerTwoEnterpriseID IS NOT NULL;
			
		-- UPDATE MANAGER LEVEL 3
		UPDATE 
			EMPHIER
		SET
			ManagerThreeFullName = EMP.FullName
			,ManagerThreeJobCode = EMP.JobCode
			,ManagerThreeJobTitle = EMP.JobTitle
			,ManagerFourEnterpriseID = EMP.ManagerEnterpriseID
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
				,ManagerEnterpriseID
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerThreeEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerThreeEnterpriseID <> ''
			AND
			EMPHIER.ManagerThreeEnterpriseID IS NOT NULL;
		
		-- UPDATE MANAGER LEVEL 4
		UPDATE 
			EMPHIER
		SET
			ManagerFourFullName = EMP.FullName
			,ManagerFourJobCode = EMP.JobCode
			,ManagerFourJobTitle = EMP.JobTitle
			,ManagerFiveEnterpriseID = EMP.ManagerEnterpriseID
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
				,ManagerEnterpriseID
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerFourEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerFourEnterpriseID <> ''
			AND
			EMPHIER.ManagerFourEnterpriseID IS NOT NULL;
			
		-- UPDATE MANAGER LEVEL 5
		UPDATE 
			EMPHIER
		SET
			ManagerFiveFullName = EMP.FullName
			,ManagerFiveJobCode = EMP.JobCode
			,ManagerFiveJobTitle = EMP.JobTitle
		FROM 
			#TBL_EmployeeHierarchyTemp EMPHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,FullName
				,JobCode
				,JobTitle
			FROM
				Employee.dbo.TBL_Employee WITH (NOLOCK)
			WHERE
				RowEndDate IS NULL
				AND
				EnterpriseID <> '') EMP
		ON
			EMPHIER.ManagerFiveEnterpriseID = EMP.EnterpriseID
		WHERE
			EMPHIER.ManagerFiveEnterpriseID <> ''
			AND
			EMPHIER.ManagerFiveEnterpriseID IS NOT NULL;
		
		-- MERGE DATA
		MERGE 
			Employee.dbo.TBL_EmployeeHierarchy AS T
		USING 
			(SELECT
				EnterpriseID
				,FullName
				,ManagerEnterpriseID
				,ManagerFullName
				,ManagerJobCode
				,ManagerJobTitle
				,ManagerTwoEnterpriseID
				,ManagerTwoFullName
				,ManagerTwoJobCode
				,ManagerTwoJobTitle
				,ManagerThreeEnterpriseID
				,ManagerThreeFullName
				,ManagerThreeJobCode
				,ManagerThreeJobTitle
				,ManagerFourEnterpriseID
				,ManagerFourFullName
				,ManagerFourJobCode
				,ManagerFourJobTitle
				,ManagerFiveEnterpriseID
				,ManagerFiveFullName
				,ManagerFiveJobCode
				,ManagerFiveJobTitle
			FROM 
				#TBL_EmployeeHierarchyTemp) AS S
		ON 
			(T.EnterpriseID = S.EnterpriseID)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				 T.FullName = S.FullName
				,T.ManagerEnterpriseID = S.ManagerEnterpriseID
				,T.ManagerFullName = S.ManagerFullName
				,T.ManagerJobCode = S.ManagerJobCode
				,T.ManagerJobTitle = S.ManagerJobTitle
				,T.ManagerTwoEnterpriseID = S.ManagerTwoEnterpriseID
				,T.ManagerTwoFullName = S.ManagerTwoFullName
				,T.ManagerTwoJobCode = S.ManagerTwoJobCode
				,T.ManagerTwoJobTitle = S.ManagerTwoJobTitle
				,T.ManagerThreeEnterpriseID = S.ManagerThreeEnterpriseID
				,T.ManagerThreeFullName = S.ManagerThreeFullName
				,T.ManagerThreeJobCode = S.ManagerThreeJobCode
				,T.ManagerThreeJobTitle = S.ManagerThreeJobTitle
				,T.ManagerFourEnterpriseID = S.ManagerFourEnterpriseID
				,T.ManagerFourFullName = S.ManagerFourFullName
				,T.ManagerFourJobCode = S.ManagerFourJobCode
				,T.ManagerFourJobTitle = S.ManagerFourJobTitle	
				,T.ManagerFiveEnterpriseID = S.ManagerFiveEnterpriseID
				,T.ManagerFiveFullName = S.ManagerFiveFullName
				,T.ManagerFiveJobCode = S.ManagerFiveJobCode
				,T.ManagerFiveJobTitle = S.ManagerFiveJobTitle
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(EnterpriseID
				,FullName
				,ManagerEnterpriseID
				,ManagerFullName
				,ManagerJobCode
				,ManagerJobTitle
				,ManagerTwoEnterpriseID
				,ManagerTwoFullName
				,ManagerTwoJobCode
				,ManagerTwoJobTitle
				,ManagerThreeEnterpriseID
				,ManagerThreeFullName
				,ManagerThreeJobCode
				,ManagerThreeJobTitle
				,ManagerFourEnterpriseID
				,ManagerFourFullName
				,ManagerFourJobCode
				,ManagerFourJobTitle
				,ManagerFiveEnterpriseID
				,ManagerFiveFullName
				,ManagerFiveJobCode
				,ManagerFiveJobTitle)
			VALUES
				(S.EnterpriseID
				,S.FullName
				,S.ManagerEnterpriseID
				,S.ManagerFullName
				,S.ManagerJobCode
				,S.ManagerJobTitle
				,S.ManagerTwoEnterpriseID
				,S.ManagerTwoFullName
				,S.ManagerTwoJobCode
				,S.ManagerTwoJobTitle
				,S.ManagerThreeEnterpriseID
				,S.ManagerThreeFullName
				,S.ManagerThreeJobCode
				,S.ManagerThreeJobTitle
				,S.ManagerFourEnterpriseID
				,S.ManagerFourFullName
				,S.ManagerFourJobCode
				,S.ManagerFourJobTitle
				,S.ManagerFiveEnterpriseID
				,S.ManagerFiveFullName
				,S.ManagerFiveJobCode
				,S.ManagerFiveJobTitle)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
		
		IF object_id('tempdb..#TBL_EmployeeHierarchyTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_EmployeeHierarchyTemp;
		END
		
		
		---- UPDATE GENERIC HIERARCHY INVERSE
		--IF object_id('tempdb..#TBL_EmployeeHierarchyInverseTemp') IS NOT NULL
		--BEGIN
		--	DROP TABLE TBL_EmployeeHierarchyInverseTemp;
		--END
		
		--CREATE TABLE 
		--	#TBL_EmployeeHierarchyInverseTemp
		--	([ManagerEnterpriseID] [varchar](28) NOT NULL,
		--	[ManagerFullName] [varchar](250) NULL,
		--	[EnterpriseID] [varchar](28) NOT NULL,
		--	[FullName] [varchar](250) NULL,
		--	[LevelDown] [int] NULL);
		
		--INSERT INTO
		--	#TBL_EmployeeHierarchyInverseTemp
		--	(ManagerEnterpriseID
		--	,ManagerFullName
		--	,EnterpriseID
		--	,FullName
		--	,LevelDown)
		--SELECT 
		--	EMPHIER.EnterpriseID AS ManagerEnterpriseID
		--	,EMPHIER.FullName As ManagerFullName
		--	,EMP.EnterpriseID
		--	,EMP.FullName
		--	,1 AS LevelDown
		--FROM
		--	Employee.dbo.TBL_EmployeeHierarchy EMPHIER WITH (NOLOCK)
		--INNER JOIN
		--	(SELECT
		--		EnterpriseID
		--		,FullName
		--		,ManagerEnterpriseID
		--	FROM
		--		Employee.dbo.TBL_Employee
		--	WHERE
		--		RowEndDate IS NULL
		--		AND
		--		EnterpriseID <> '') EMP
		--ON
		--	EMPHIER.EnterpriseID = EMP.ManagerEnterpriseID;
			
		--DECLARE @leveldown INT

		--SET @leveldown = 2

		--WHILE (@leveldown < 12)

		--BEGIN
			
		--	INSERT INTO
		--		#TBL_EmployeeHierarchyInverseTemp
		--		(ManagerEnterpriseID
		--		,ManagerFullName
		--		,EnterpriseID
		--		,FullName
		--		,LevelDown)
		--	SELECT 
		--		EMPHIERINV.ManagerEnterpriseID
		--		,ManagerFullName
		--		,EMP.EnterpriseID
		--		,EMP.FullName
		--		,@leveldown AS LevelDown
		--	FROM
		--		#TBL_EmployeeHierarchyInverseTemp EMPHIERINV WITH (NOLOCK)
		--	INNER JOIN
		--		(SELECT
		--			EnterpriseID
		--			,FullName
		--			,ManagerEnterpriseID
		--		FROM
		--			Employee.dbo.TBL_Employee EMP
		--		WHERE
		--			RowEndDate IS NULL
		--			AND
		--			EnterpriseID <> '') EMP
		--	ON
		--		EMPHIERINV.EnterpriseID = EMP.ManagerEnterpriseID
		--	LEFT JOIN
		--		(SELECT
		--			ManagerEnterpriseID
		--			,EnterpriseID
		--		FROM
		--			#TBL_EmployeeHierarchyInverseTemp) EMPHIERINVPREV
		--	ON
		--		EMPHIERINV.ManagerEnterpriseID = EMPHIERINVPREV.ManagerEnterpriseID
		--		AND
		--		EMP.EnterpriseID = EMPHIERINVPREV.EnterpriseID
		--	WHERE
		--		EMPHIERINVPREV.EnterpriseID IS NOT NULL
		--		AND
		--		EMP.EnterpriseID IS NOT NULL
		--		AND
		--		LevelDown = @leveldown - 1
			
		--	SET
		--		@leveldown = (@leveldown + 1)
		--END;
		
		---- MERGE DATA
		--MERGE 
		--	Employee.dbo.TBL_EmployeeHierarchyInverse AS T
		--USING 
		--	(SELECT
		--		ManagerEnterpriseID
		--		,ManagerFullName
		--		,EnterpriseID
		--		,FullName
		--		,LevelDown
		--	FROM 
		--		#TBL_EmployeeHierarchyInverseTemp) AS S
		--ON 
		--	(T.ManagerEnterpriseID = S.ManagerEnterpriseID
		--	AND
		--	T.EnterpriseID = S.EnterpriseID)
		--WHEN MATCHED THEN
		--	UPDATE 
		--	SET 
		--		 T.ManagerEnterpriseID = S.ManagerEnterpriseID
		--		,T.ManagerFullName = S.ManagerFullName
		--		,T.EnterpriseID = S.EnterpriseID
		--		,T.FullName = S.FullName
		--		,T.LevelDown = S.LevelDown
		--WHEN NOT MATCHED BY TARGET THEN
		--	INSERT 
		--		(ManagerEnterpriseID
		--		,ManagerFullName
		--		,EnterpriseID
		--		,FullName
		--		,LevelDown)
		--	VALUES
		--		(S.ManagerEnterpriseID
		--		,S.ManagerFullName
		--		,S.EnterpriseID
		--		,S.FullName
		--		,S.LevelDown)
		--WHEN NOT MATCHED BY SOURCE THEN
		--	DELETE;
		
		--IF object_id('tempdb..#TBL_EmployeeHierarchyInverseTemp') IS NOT NULL
		--BEGIN
		--	DROP TABLE #TBL_EmployeeHierarchyInverseTemp;
		--END
		
		-- UPDATE IN-HOME FIELD HIERARCHY
		IF object_id('tempdb..#TBL_EmployeeHierarchyIHTechRegionTemp') IS NOT NULL
		BEGIN
			DROP TABLE TBL_EmployeeHierarchyIHTechRegionTemp;
		END
		
		CREATE TABLE 
			#TBL_EmployeeHierarchyIHTechRegionTemp
			([EnterpriseID] [varchar](10) NOT NULL,
			[NPSID] [varchar](7) NULL,
			[FullName] [varchar](250) NULL,
			[TMEnterpriseID] [varchar](10) NULL,
			[TMFullName] [varchar](250) NULL,
			[DSMEnterpriseID] [varchar](10) NULL,
			[DSMFullName] [varchar](250) NULL,
			[Unit] [char](7) NULL,
			[UnitName] [varchar](250) NULL,
			[TFDEnterpriseID] [varchar](10) NULL,
			[TFDFullName] [varchar](250) NULL,
			[TerritoryName] [varchar](250) NULL,
			[Region] [char](7) NULL,
			[RegionName] [varchar](250) NULL,
			[RVPEnterpriseID] [varchar](10) NULL,
			[RVPFullName] [varchar](250) NULL);
		
		-- GL Department numbers listed below should be for all In-Home Techs
		INSERT INTO
			#TBL_EmployeeHierarchyIHTechRegionTemp
			(EnterpriseID
			,NPSID
			,FullName
			,Unit
			,UnitName
			,Region
			,RegionName)
		SELECT
			EnterpriseID
			,CASE WHEN NPSID = '' THEN SalesID ELSE NPSID END AS NPSID
			,FullName
			,EMP.Unit
			,UnitName
			,EMP.Region
			,RegionName
		FROM
			Employee.dbo.TBL_Employee EMP WITH (NOLOCK)
		LEFT JOIN
			(SELECT
				Unit
				,UnitName
			FROM
				Employee.dbo.TBL_NPSBaseUnit WITH (NOLOCK)
			WHERE
				Unit <> '') UNIT
		ON
			EMP.Unit = UNIT.Unit
		LEFT JOIN
			(SELECT
				Unit AS Region
				,UnitName AS RegionName
			FROM
				Employee.dbo.TBL_NPSBaseUnit WITH (NOLOCK)
			WHERE
				Unit <> '') REG
		ON
			EMP.Region = REG.Region
		WHERE
			RowEndDate IS NULL
			AND
			EnterpriseID <> ''
			AND
			GLDepartment IN ('3925','3927','3935')
			AND
			EMP.Region IN ('0000820', '0000830', '0000890');  -- DO NOT INCLUDE HVAC OR STAC AS IT DOESNT FOLLOW INHOME HIERARCHY
		
		-- UPDATE RVP
		UPDATE
			#TBL_EmployeeHierarchyIHTechRegionTemp
		SET
			RVPEnterpriseID = CASE 
								WHEN Region = '0000820' THEN 'jmorg07'
								WHEN Region = '0000830' THEN 'cgeog'
								WHEN Region = '0000890' THEN 'mirby2'
							END
			,RVPFullName = CASE 
								WHEN Region = '0000820' THEN 'Jeff E Morgan'
								WHEN Region = '0000830' THEN 'Clinton Geog'
								WHEN Region = '0000890' THEN 'Mike D Irby'
							END;
		
		-- UPDATE TM
		UPDATE 
			EMPIHFHIER
		SET
			TMEnterpriseID = EMP.TMEnterpriseID
			,TMFullName = EMP.TMFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,CASE
					WHEN ManagerJobCode = 'HS1202' THEN ManagerEnterpriseID
					WHEN ManagerTwoJobCode = 'HS1202' THEN ManagerTwoEnterpriseID
					WHEN ManagerThreeJobCode = 'HS1202' THEN ManagerThreeEnterpriseID
					WHEN ManagerFourJobCode = 'HS1202' THEN ManagerFourEnterpriseID
					WHEN ManagerFiveJobCode = 'HS1202' THEN ManagerFiveEnterpriseID
				END AS TMEnterpriseID
				,CASE
					WHEN ManagerJobCode = 'HS1202' THEN ManagerFullName
					WHEN ManagerTwoJobCode = 'HS1202' THEN ManagerTwoFullName
					WHEN ManagerThreeJobCode = 'HS1202' THEN ManagerThreeFullName
					WHEN ManagerFourJobCode = 'HS1202' THEN ManagerFourFullName
					WHEN ManagerFiveJobCode = 'HS1202' THEN ManagerFiveFullName
				END AS TMFullName
			FROM
				Employee.dbo.TBL_EmployeeHierarchy WITH (NOLOCK)
			WHERE
				ManagerJobCode = 'HS1202'
				OR
				ManagerTwoJobCode = 'HS1202'
				OR
				ManagerThreeJobCode = 'HS1202'
				OR
				ManagerFourJobCode = 'HS1202'
				OR
				ManagerFiveJobCode = 'HS1202') EMP
		ON
			EMPIHFHIER.EnterpriseID = EMP.EnterpriseID
		WHERE
			EMPIHFHIER.EnterpriseID <> ''
			AND
			EMPIHFHIER.EnterpriseID IS NOT NULL;
			
		-- UPDATE DSM
		UPDATE 
			EMPIHFHIER
		SET
			DSMEnterpriseID = EMP.DSMEnterpriseID
			,DSMFullName = EMP.DSMFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,CASE
					WHEN ManagerJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerEnterpriseID
					WHEN ManagerTwoJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerTwoEnterpriseID
					WHEN ManagerThreeJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerThreeEnterpriseID
					WHEN ManagerFourJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFourEnterpriseID
					WHEN ManagerFiveJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFiveEnterpriseID
				END AS DSMEnterpriseID
				,CASE
					WHEN ManagerJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFullName
					WHEN ManagerTwoJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerTwoFullName
					WHEN ManagerThreeJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerThreeFullName
					WHEN ManagerFourJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFourFullName
					WHEN ManagerFiveJobCode IN ('HS1201', 'HS9678', 'HS9802') THEN ManagerFiveFullName
				END AS DSMFullName
			FROM
				Employee.dbo.TBL_EmployeeHierarchy WITH (NOLOCK)
			WHERE
				ManagerJobCode IN ('HS1201', 'HS9678', 'HS9802')
				OR
				ManagerTwoJobCode IN ('HS1201', 'HS9678', 'HS9802')
				OR
				ManagerThreeJobCode IN ('HS1201', 'HS9678', 'HS9802')
				OR
				ManagerFourJobCode IN ('HS1201', 'HS9678', 'HS9802')
				OR
				ManagerFiveJobCode IN ('HS1201', 'HS9678', 'HS9802')) EMP
		ON
			EMPIHFHIER.EnterpriseID = EMP.EnterpriseID
		WHERE
			EMPIHFHIER.EnterpriseID <> ''
			AND
			EMPIHFHIER.EnterpriseID IS NOT NULL;
		
		-- UPDATE DSM FOR UNITS WITH ONE DSM
		UPDATE 
			EMPIHFHIER
		SET
			DSMEnterpriseID = EMP.DSMEnterpriseID
			,DSMFullName = EMP.DSMFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT DISTINCT
				DSMEnterpriseID
				,DSMFullName
				,DSMEMP.Unit
			FROM
				#TBL_EmployeeHierarchyIHTechRegionTemp DSMEMP
			INNER JOIN
				(SELECT
					Unit
					,COUNT(DSMEnterpriseID) AS DSMCount
				FROM
					(SELECT DISTINCT
						Unit
						,DSMEnterpriseID
					FROM
						#TBL_EmployeeHierarchyIHTechRegionTemp
					WHERE
						DSMEnterpriseID IS NOT NULL) DSM
				GROUP BY
					Unit
				HAVING 
					COUNT(DSMEnterpriseID) = 1) DSMCOUNT
			ON
				DSMEMP.Unit = DSMCOUNT.Unit
			WHERE
				DSMEnterpriseID IS NOT NULL) EMP
		ON
			EMPIHFHIER.Unit = EMP.Unit
		WHERE
			EMPIHFHIER.DSMEnterpriseID IS NULL;
		
		-- UPDATE TFD
		UPDATE 
			EMPIHFHIER
		SET
			TFDEnterpriseID = EMP.TFDEnterpriseID
			,TFDFullName = EMP.TFDFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT
				EnterpriseID
				,CASE
					WHEN ManagerJobCode = 'HS9666' THEN ManagerEnterpriseID
					WHEN ManagerTwoJobCode = 'HS9666' THEN ManagerTwoEnterpriseID
					WHEN ManagerThreeJobCode = 'HS9666' THEN ManagerThreeEnterpriseID
					WHEN ManagerFourJobCode = 'HS9666' THEN ManagerFourEnterpriseID
					WHEN ManagerFiveJobCode = 'HS9666' THEN ManagerFiveEnterpriseID
				END AS TFDEnterpriseID
				,CASE
					WHEN ManagerJobCode = 'HS9666' THEN ManagerFullName
					WHEN ManagerTwoJobCode = 'HS9666' THEN ManagerTwoFullName
					WHEN ManagerThreeJobCode = 'HS9666' THEN ManagerThreeFullName
					WHEN ManagerFourJobCode = 'HS9666' THEN ManagerFourFullName
					WHEN ManagerFiveJobCode = 'HS9666' THEN ManagerFiveFullName
				END AS TFDFullName
			FROM
				Employee.dbo.TBL_EmployeeHierarchy WITH (NOLOCK)
			WHERE
				ManagerJobCode = 'HS9666'
				OR
				ManagerTwoJobCode = 'HS9666'
				OR
				ManagerThreeJobCode = 'HS9666'
				OR
				ManagerFourJobCode = 'HS9666'
				OR
				ManagerFiveJobCode = 'HS9666') EMP
		ON
			EMPIHFHIER.EnterpriseID = EMP.EnterpriseID
		WHERE
			EMPIHFHIER.EnterpriseID <> ''
			AND
			EMPIHFHIER.EnterpriseID IS NOT NULL;		
		
		-- UPDATE TFD TO MAKE SURE ALL EMPLOYEES IN UNIT
		-- HAVE SAME TFD
		UPDATE 
			EMPIHFHIER
		SET
			TFDEnterpriseID = EMP.TFDEnterpriseID
			,TFDFullName = EMP.TFDFullName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT DISTINCT
				TFDEnterpriseID
				,TFDFullName
				,TFDEMP.Unit
			FROM
				#TBL_EmployeeHierarchyIHTechRegionTemp TFDEMP
			INNER JOIN
				(SELECT
					Unit
					,COUNT(TFDEnterpriseID) AS TFDCount
				FROM
					(SELECT DISTINCT
						Unit
						,TFDEnterpriseID
					FROM
						#TBL_EmployeeHierarchyIHTechRegionTemp
					WHERE
						TFDEnterpriseID IS NOT NULL) TFD
				GROUP BY
					Unit
				HAVING 
					COUNT(TFDEnterpriseID) = 1) TFDCOUNT
			ON
				TFDEMP.Unit = TFDCOUNT.Unit
			WHERE
				TFDEnterpriseID IS NOT NULL) EMP
		ON
			EMPIHFHIER.Unit = EMP.Unit
		WHERE
			EMPIHFHIER.TFDEnterpriseID IS NULL;
		
		-- UPDATE TERRITORY NAME
		UPDATE 
			EMPIHFHIER
		SET
			TerritoryName = UNIT.TerritoryName
		FROM 
			#TBL_EmployeeHierarchyIHTechRegionTemp EMPIHFHIER
		INNER JOIN
			(SELECT
				Unit
				,TerritoryName
			FROM
				Employee.dbo.TBL_InHomeUnitTerritory WITH (NOLOCK)) UNIT
		ON
			EMPIHFHIER.Unit = UNIT.Unit;
		
		-- MERGE DATA
		MERGE 
			Employee.dbo.TBL_EmployeeHierarchyIHTechRegion AS T
		USING 
			(SELECT
				EnterpriseID
				,NPSID
				,FullName
				,TMEnterpriseID
				,TMFullName
				,DSMEnterpriseID
				,DSMFullName
				,Unit
				,UnitName
				,TFDEnterpriseID
				,TFDFullName
				,TerritoryName
				,Region
				,RegionName
				,RVPEnterpriseID
				,RVPFullName
			FROM 
				#TBL_EmployeeHierarchyIHTechRegionTemp) AS S
		ON 
			(T.EnterpriseID = S.EnterpriseID)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				 T.NPSID = S.NPSID
				,T.FullName = S.FullName
				,T.TMEnterpriseID = S.TMEnterpriseID
				,T.TMFullName = S.TMFullName
				,T.DSMEnterpriseID = S.DSMEnterpriseID
				,T.DSMFullName = S.DSMFullName
				,T.Unit = S.Unit
				,T.UnitName = S.UnitName
				,T.TFDEnterpriseID = S.TFDEnterpriseID
				,T.TFDFullName = S.TFDFullName
				,T.TerritoryName = S.TerritoryName
				,T.Region = S.Region
				,T.RegionName = S.RegionName
				,T.RVPEnterpriseID = S.RVPEnterpriseID
				,T.RVPFullName = S.RVPFullName
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(EnterpriseID
				,NPSID
				,FullName
				,TMEnterpriseID
				,TMFullName
				,DSMEnterpriseID
				,DSMFullName
				,Unit
				,UnitName
				,TFDEnterpriseID
				,TFDFullName
				,TerritoryName
				,Region
				,RegionName
				,RVPEnterpriseID
				,RVPFullName)
			VALUES
				(S.EnterpriseID
				,S.NPSID
				,S.FullName
				,S.TMEnterpriseID
				,S.TMFullName
				,S.DSMEnterpriseID
				,S.DSMFullName
				,S.Unit
				,S.UnitName
				,S.TFDEnterpriseID
				,S.TFDFullName
				,S.TerritoryName
				,S.Region
				,S.RegionName
				,S.RVPEnterpriseID
				,S.RVPFullName)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
		
		-- MERGE DATA FOR TBL_EmployeeHierarchyIHUnitRegion
		MERGE 
			Employee.dbo.TBL_EmployeeHierarchyIHUnitRegion AS T
		USING 
			(SELECT DISTINCT
				Unit
				,UnitName
				,TFDEnterpriseID
				,TFDFullName
				,TerritoryName
				,Region
				,RegionName
				,RVPEnterpriseID
				,RVPFullName
			FROM 
				#TBL_EmployeeHierarchyIHTechRegionTemp) AS S
		ON 
			(T.Unit = S.Unit)
		WHEN MATCHED THEN
			UPDATE 
			SET 
				 T.Unit = S.Unit
				,T.UnitName = S.UnitName
				,T.TFDEnterpriseID = S.TFDEnterpriseID
				,T.TFDFullName = S.TFDFullName
				,T.TerritoryName = S.TerritoryName
				,T.Region = S.Region
				,T.RegionName = S.RegionName
				,T.RVPEnterpriseID = S.RVPEnterpriseID
				,T.RVPFullName = S.RVPFullName
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Unit
				,UnitName
				,TFDEnterpriseID
				,TFDFullName
				,TerritoryName
				,Region
				,RegionName
				,RVPEnterpriseID
				,RVPFullName)
			VALUES
				(S.Unit
				,S.UnitName
				,S.TFDEnterpriseID
				,S.TFDFullName
				,S.TerritoryName
				,S.Region
				,S.RegionName
				,S.RVPEnterpriseID
				,S.RVPFullName)
		WHEN NOT MATCHED BY SOURCE THEN
			DELETE;
			
			
		IF object_id('tempdb..#TBL_EmployeeHierarchyIHTechRegionTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_EmployeeHierarchyIHTechRegionTemp;
		END
		
	END