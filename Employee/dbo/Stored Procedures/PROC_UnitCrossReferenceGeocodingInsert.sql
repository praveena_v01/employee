﻿-- =============================================
-- Author:		Aaron Cote
-- Create date: 01/26/2017
-- Description:	For saving the geocoordinates of a unit address
-- =============================================
CREATE PROCEDURE [dbo].[PROC_UnitCrossReferenceGeocodingInsert]
	@Unit char(7) = NULL,
	@InputAddress varchar(500) = NULL,
	@PlaceID varchar(300) = NULL,
	@StreetNumber varchar(300) = NULL,
	@StreetNumberLong varchar(300) = NULL,
	@Route varchar(300) = NULL,
	@RouteLong varchar(300) = NULL,
	@SubPremise varchar(300) = NULL,
	@SubPremiseLong varchar(300) = NULL,
	@Neighborhood varchar(300) = NULL,
	@NeighborhoodLong varchar(300) = NULL,
	@Locality varchar(300) = NULL,
	@LocalityLong varchar(300) = NULL,
	@AdministrativeAreaLevelThree varchar(300) = NULL,
	@AdministrativeAreaLevelThreeLong varchar(300) = NULL,
	@AdministrativeAreaLevelTwo varchar(300) = NULL,
	@AdministrativeAreaLevelTwoLong varchar(300) = NULL,
	@AdministrativeAreaLevelOne varchar(300) = NULL,
	@AdministrativeAreaLevelOneLong varchar(300) = NULL,
	@Country varchar(300) = NULL,
	@CountryLong varchar(300) = NULL,
	@PostalCode varchar(300) = NULL,
	@PostalCodeLong varchar(300) = NULL,
	@PostalCodeSuffix varchar(300) = NULL,
	@PostalCodeSuffixLong varchar(300) = NULL,
	@FormattedAddress varchar(300) = NULL,
	@LocationType varchar(300) = NULL,
	@Latitude decimal(17,13) = NULL,
	@Longitude decimal(17,13) = NULL,
	@NorthEastLatitude decimal(17,13) = NULL,
	@NorthEastLongitude decimal(17,13) = NULL,
	@SouthWestLatitude decimal(17,13) = NULL,
	@SouthWestLongitude decimal(17,13) = NULL,

	@errorcode VARCHAR(150) = NULL OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- Interfering with SELECT statements.
	SET NOCOUNT ON;
	
	-- Merge into primary table
	INSERT INTO
		Employee.dbo.TBL_UnitCrossReferenceGeocoding
		(Unit
		,InputAddress
		,PlaceID
		,StreetNumber
		,StreetNumberLong
		,[Route]
		,RouteLong
		,SubPremise
		,SubPremiseLong
		,Neighborhood
		,NeighborhoodLong
		,Locality
		,LocalityLong
		,AdministrativeAreaLevelThree
		,AdministrativeAreaLevelThreeLong
		,AdministrativeAreaLevelTwo
		,AdministrativeAreaLevelTwoLong
		,AdministrativeAreaLevelOne
		,AdministrativeAreaLevelOneLong
		,Country
		,CountryLong
		,PostalCode
		,PostalCodeLong
		,PostalCodeSuffix
		,PostalCodeSuffixLong
		,FormattedAddress
		,LocationType
		,Latitude
		,Longitude
		,Location
		,NorthEastLatitude
		,NorthEastLongitude
		,SouthWestLatitude
		,SouthWestLongitude
		,InsertDate)
	VALUES
		(@Unit
		,@InputAddress
		,@PlaceID
		,@StreetNumber
		,@StreetNumberLong
		,@Route
		,@RouteLong
		,@SubPremise
		,@SubPremiseLong
		,@Neighborhood
		,@NeighborhoodLong
		,@Locality
		,@LocalityLong
		,@AdministrativeAreaLevelThree
		,@AdministrativeAreaLevelThreeLong
		,@AdministrativeAreaLevelTwo
		,@AdministrativeAreaLevelTwoLong
		,@AdministrativeAreaLevelOne
		,@AdministrativeAreaLevelOneLong
		,@Country
		,@CountryLong
		,@PostalCode
		,@PostalCodeLong
		,@PostalCodeSuffix
		,@PostalCodeSuffixLong
		,@FormattedAddress
		,@LocationType
		,@Latitude
		,@Longitude
		,geography::Point(@Latitude, @Longitude, 4326)
		,@NorthEastLatitude
		,@NorthEastLongitude
		,@SouthWestLatitude
		,@SouthWestLongitude
		,GETDATE())
END