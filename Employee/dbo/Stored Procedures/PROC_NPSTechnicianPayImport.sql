﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PROC_NPSTechnicianPayImport]

AS
	BEGIN
	
		DECLARE @querystring NVARCHAR(MAX) = NULL;
		
		IF object_id('tempdb..#TBL_NPSTechnicianPay') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSTechnicianPayTemp;
		END
		
		CREATE TABLE 
			#TBL_NPSTechnicianPayTemp
			(Unit [varchar] (7) NOT NULL,
			NPSID [varchar](7) NOT NULL,
			EmployeeID [varchar](11) NULL,
			EmployeePay [decimal](7, 3) NULL);
		
		
		SET @querystring = 'INSERT INTO #TBL_NPSTechnicianPayTemp (Unit, NPSID, EmployeeID, EmployeePay)'

		SET @querystring = @querystring + ' SELECT svc_un_no, svc_tec_emp_id_no, crp_emp_id_no, tec_hry_py_rt FROM OPENQUERY(TERADATAPROD, '

		SET @querystring = @querystring + ' ''SELECT svc_un_no, svc_tec_emp_id_no, crp_emp_id_no, tec_hry_py_rt FROM HS_DAILY_VIEWS.NPSXTPY;'''

		SET @querystring = @querystring + ')'
		
		-- Populate temp table.
		EXEC(@querystring);
		
		
		
		-- Merge into primary table
		MERGE 
			Employee.dbo.TBL_NPSTechnicianPay AS T
		USING 
			(SELECT
			   Unit
			  ,NPSID			  
			  ,EmployeeID
			  ,EmployeePay			 
			FROM 
				#TBL_NPSTechnicianPayTemp) AS S
		ON 
			(T.Unit = S.Unit 
			AND
			T.EmployeeID = S.NPSID)
			
		WHEN MATCHED THEN
			UPDATE 
			SET
				T.Unit = S.Unit
				,T.NPSID = S.NPSID
				,T.EmployeeID = S.EmployeeID
				,T.EmployeePay = S.EmployeePay				
		WHEN NOT MATCHED BY TARGET THEN
			INSERT 
				(Unit
				,NPSID
				,EmployeeID				
				,EmployeePay)
			VALUES 
				(S.Unit
				,S.NPSID
				,S.EmployeeID
				,S.EmployeePay);
				
		IF object_id('tempdb..#TBL_NPSTechnicianPayTemp') IS NOT NULL
		BEGIN
			DROP TABLE #TBL_NPSTechnicianPayTemp;
		END
END